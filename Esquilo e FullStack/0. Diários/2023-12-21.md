A atividade de hoje será o início da atividade do home office.

Eu quero que vocês planejem e construam um site em html que contenha as especificações que vou listar. O tema do site deve ser a programação em HTML e CSS, ou seja, escreva sobre a funcionalidade de alguma tag, comando, estrutura, estilo, etc. Pesquise na Internet sobre o tema.

Usem e abusem da pesquisa para aprenderem a usar os comandos. Aqui estão algumas sugestões de sites para consulta:
- mozila eveloper
- w3
- stackoverflow


As especificações do site seguem abaixo, tente completar elas em ordemm de maneira que você garanta que que o máximo dela esteja completo:

- A estrutura geral de pelo menos uma das páginas deve ser:
	- Um cabeçalho (header).
	- Um texto de no mínimo X caracteres.
		- Algumas palavras do texto devem estar estilizadas, cada um dos seguintes comandos devem ser usado pelo menos uma vez:
	- Uma imagem posicionada ao lado do texto quando a tela está grande, e abaixo do texto quando está no modo celular.
	- Um roda pé.
- - Além disso, o site deve ser bonito, esteticamente agradável, os textos e imagens devem estar bem posicionados, as cores devem fazer sentido umas com as outras:
	- Material de apoio:
-  Deve conter pelo menos 1 imagem.
- Pelo menos uma das páginas deve ter seu conteúdo organizado em 4 blocos; containers diferentes, disposto em duas colunas e duas linhas. 
	- Essa tarefa deve ser feita usando o FlexBox ou o Grid
- Deve conter pelo menos duas páginas, e elas devem ter botões/links que levam uma para a outra.
	- Sugestão de comandos:
- Deve conter pelo menos um link para um site extern.
	- Sugestão de comando:
- Deve ser responsivo para pelo menos 2 dimensões diferentes de mídias( exemplo: . . . ).
	- Para testar a responsividade, use o modo desenvolvedor do seu navegador (ex.; abra o site no google chormie, clique F12 e clique na opção de responsividade no topo direito da tela)
	- O comando Media Query deve ser usado pelo menos uma vez
-  Um botão que realize uma conta matemática e publique o resultado numa janela pop-up:
	- Sugestão:
-

- 