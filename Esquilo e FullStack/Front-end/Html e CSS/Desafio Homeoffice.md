
---

# Desafio Homeoffice

A proposta da atividade é o planejamento e a construção de um site em HTML que contenha as especificações que listadas abaixo. O tema do site deve ser a programação em HTML e CSS, ou seja, sobre a funcionalidade de alguma tag, comando, estrutura, estilo, etc. Pesquise na Internet sobre o tema. Caso você queira fazer sobre algum outro tema, fique a vontade, o importante é se atentar a forma do site, não tanto ao conteúdo.
	- Sugestão de conteúdo: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_box_model/Introduction_to_the_CSS_box_model

Usem e abusem da pesquisa para aprenderem a usar os comandos. Aqui estão algumas sugestões de sites para consulta:
- Mozilla Developer: https://developer.mozilla.org/pt-BR/docs/Web/HTML
- W3 - https://www.w3schools.com/
- Stack Overflow - https://stackoverflow.com/

As especificações do site seguem abaixo, tentem completar um desafios de cada vez. Dica: aso não consiga resolver algum deles, pule para o próximo e volte mais tarde.

### Especificações

1. O código CSS deve estar em um arquivo separado do HTML:
	1. Ou seja, todo conteúdo de estilo deve estar organizado em um mesmo arquivo .css, evite deixar comandos dentro de  `<style>` no arquivo .html;
2. A estrutura geral de pelo menos uma das páginas deve ser:
	- Um cabeçalho (`<header>`) e um roda pé (`<footer>`);
	- Um texto de no mínimo 1050 caracteres, ou aproximadamente 200 palavras;
		- Contador de palavras: https://www.invertexto.com/contador-caracteres
	- Algumas palavras do texto devem estar estilizadas com os seguintes comandos (use cada um deles pelo menos uma vez):
			- Usando `<text-decoration>`, faça uma linha pontilhada, colorida com espessura maior do que 1px, a cima de uma palavra ou frase;
				- Mais informações em: https://developer.mozilla.org/pt-BR/docs/Web/CSS/text-decoration ;
			- Usando text-shadow, crie uma sombra colorida em uma palavra ou frase:
				- Tente brincar com a posição da sombra alterando os valores de `<offset-x>` e `<offset-y>`;
				- Mais informações em: https://developer.mozilla.org/pt-BR/docs/Web/CSS/text-shadow ;
			- Altere a fonte do texto usando `<font-family>`;
			- Organize a disposição do texto usando `<text-wrap>`;
				1. 1. https://developer.mozilla.org/en-US/docs/Web/CSS/text-wrap ;	
			- Use os comandos `<hr>`(header row/linha horizontal) para criar uma linha horizontal- `<br>` (break row) para criar uma quebra de linha e `<pre>` para que o texto seja exibido da mesma maneira em que foi disposto no arquivo (espaços em branco são mantidos no texto da mesma forma em que este foi digitado);
			- Se quiser testar outras estilizações, tente usar as tags `<mark>`, `<em>`, `<strong>`, `<sub>`, `<del>`,`<q>` e `<blockquote cite="">`;
3. Além disso, o site deve ser bonito, esteticamente agradável, os textos e imagens devem estar bem posicionados, as cores devem fazer sentido umas com as outras. Abaixo, alguns materiais de apoio para o estudo, não precisam usar as cores e estilos indicados, eles são apenas modelos, ideias para inspiração.
	- Paleta de cores para 2024: https://www.pantone.com.br/paletas-de-cores-cor-do-ano-2024/ .
	- Sugestões de fontes, tipografias: https://www.hostinger.com.br/tutoriais/melhores-fontes-html ;
	- Layouts: https://www.w3schools.com/html/html_layout.asp
	- Dicas de Layouts: https://www.dio.me/articles/guia-para-organizacao-de-layout-em-html-dicas-e-melhores-praticas
4. Deve conter pelo menos 1 imagem, inserida com `<img>`;
5. Pelo menos uma das páginas deve ter seu conteúdo organizado em 4 blocos/containers diferentes, disposto em duas colunas e duas linhas. 
	- Essa tarefa deve ser feita usando o FlexBox ou o Grid;
	- Dicas de como usar o flex-box e o grid: https://www.alura.com.br/artigos/css-guia-do-flexbox?utm_term=&utm_campaign=%5BSearch%5D+%5BPerformance%5D+-+Dynamic+Search+Ads+-+Artigos+e+Conte%C3%BAdos&utm_source=adwords&utm_medium=ppc&hsa_acc=7964138385&hsa_cam=11384329873&hsa_grp=111087461203&hsa_ad=682526577071&hsa_src=g&hsa_tgt=aud-539280195044:dsa-843358956400&hsa_kw=&hsa_mt=&hsa_net=adwords&hsa_ver=3&gad_source=1&gclid=EAIaIQobChMI1KPSi8mggwMVyxqtBh0GAwAvEAAYASAAEgKuDfD_BwE
6. Deve conter pelo menos duas páginas, e elas devem ter botões/links que levam uma para a outra. Os comandos `<nav>`, `<a>` e `<botton>` podem ser usados em conjunto para esse fim;
	- Mais informações:
		- https://developer.mozilla.org/pt-BR/docs/Web/HTML/Element/nav
		- https://www.kadunew.com/blog/html/como-usar-o-elemento-nav-do-html5
7. Deve conter pelo menos um link para um site extero usando `<a>`:
	- Mais informações em: https://developer.mozilla.org/pt-BR/docs/Web/HTML/Element/a ;
	- Você pode também tentar inserir um link em uma image, por exemplo:, combinando os comandos `<a>` e `<img>`;
8. Deve ser responsivo para pelo menos 1 dimensão diferente de mídia ( exemplo: iphone SE).
	- Para testar a responsividade, use o modo desenvolvedor do seu navegador (ex.; abra o site no google chormie, clique F12 e clique na opção de responsividade no topo direito da tela);
	- O comando Media Queries deve ser usado pelo menos uma vez;
		- Mais informações em: https://developer.mozilla.org/pt-BR/docs/Web/CSS/CSS_media_queries/Using_media_queries
9. Um botão que realize uma conta matemática e publique o resultado numa janela pop-up. Use um programa produzido em aula anteriormente.
10. Se você tiver realizado todas as tarefas, tente influir um link para uma mapa, ou outro site, usando o comando `<inframe>`.