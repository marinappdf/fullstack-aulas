<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tabela CSS</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <table class="table">
            <tr>
                <th>Coluna 1</th>
                <th>Coluna 2</th>
                <th>Coluna 3</th>
                <th>Coluna 4</th>
            </tr>
            <tr>
                <td>Dado 1</td>
                <td>Dado 2</td>
                <td>Dado 3</td>
                <td>Dado 4</td>
            </tr>
            <!-- Adicione mais linhas conforme necessário -->
        </table>
    </div>
</body>
</html>