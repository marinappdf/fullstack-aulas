
# Inglês instrumental

### Marina de Freitas
#### @marifreitas___

---

### Hoje vamos aprender o significado de palavras em inglês muito usadas na programação

### Para isso, vamos fazer uma Tabela, usando html, que contenha uma coluna com palavras em inglês e outra coluna com a tradução

### A tabela deve ser bonita, o texto formatado, letra bonita, hover bonitinho, enfim, tem que ser apresentável.

---

## Alguns comandos básicos para criar uma tabela:

- `<table>`: Define a tabela.
- `<tr>`: Define uma linha na tabela.
- `<td>`: Define uma célula na linha da tabela.
- `<th>`: Define uma célula de cabeçalho na linha da tabela (opcional, usada tipicamente na primeira linha ou primeira coluna para cabeçalhos).


---

### Exemplo

<!DOCTYPE html>
<html>
> 	<head>
    <title>Tabela em HTML</title>
</head>
<body>
<h2>Exemplo de Tabela em HTML</h2>

<table border="1">
    <tr>
        <th>Coluna 1</th>
        <th>Coluna 2</th>
        <th>Coluna 3</th>
    </tr>
    <tr>
        <td>Linha 1, Coluna 1</td>
        <td>Linha 1, Coluna 2</td>
        <td>Linha 1, Coluna 3</td>
    </tr>
    <tr>
        <td>Linha 2, Coluna 1</td>
        <td>Linha 2, Coluna 2</td>
        <td>Linha 2, Coluna 3</td>
    </tr>
</table>

</body>
</html>



--

### EXEMPLO DE CÓDIGO


"<!DOCTYPE html>
<html>
<head>
    <title>Tabela em HTML</title>
</head>
<body>

<h2>Exemplo de Tabela em HTML</h2>

<table border="1">
    <tr>
        <th>Coluna 1</th>
        <th>Coluna 2</th>
        <th>Coluna 3</th>
    </tr>
    <tr>
        <td>Linha 1, Coluna 1</td>
        <td>Linha 1, Coluna 2</td>
        <td>Linha 1, Coluna 3</td>
    </tr>
    <tr>
        <td>Linha 2, Coluna 1</td>
        <td>Linha 2, Coluna 2</td>
        <td>Linha 2, Coluna 3</td>
    </tr>
</table>

</body>
</html>
"

---

## Lista de palavras

## **HTML**
   - HTML
   - body
   - div
   - paragraph
   - anchor
   - image
   - unordered list
   - list item
   - input
   - button
   - form
   - header
   - footer
   - span
   - table
   - table row
   - table data
   - heading
   - video
   - audio
---
## **CSS**
   - color
   - font-size
   - margin
   - padding
   - border
   - display
   - position
   - float
   - background
   - width
   - height
   - flex
   - grid
   - opacity
   - transition
   - box-shadow
   - text-align
   - z-index
   - overflow
---
## **JavaScript**
   - let
   - const
   - function
   - if
   - else
   - for
   - while
   - return
   - Array
   - Object
   - Promise
   - fetch
   - addEventListener
   - querySelector
   - innerHTML
   - createElement
   - appendChild
   - classList
   - JSON
   - map

---
| Palavra            | Palavra        | Palavra    | Palavra    | Palavra   |
|--------------------|----------------|------------|------------|-----------|
| main           | color          | let        | div        | transition|
| body               | font-size      | const      | padding    | z-index   |
| paragraph          | margin         | function   | border     | overflow  |
| anchor             | display        | if         | position   | box-shadow|
| image              | float          | else       | background | text-align|
| unordered list     | width          | for        | opacity    | inner |
| list item          | height         | while      | Array      | create |
| input              | flex           | return     | Object     | append |
| button             | grid           | Promise    | fetch      | class |
| form               | opacity        | add Event Listener | child | Element  |
| header             | transition     | query Selector | inner | map       |
| footer             | box-shadow     |    heading         |   overflow          |  script         |
| span               | text-align     |    table row               |       table data      |           |
| table              | z-index        |  input          |       label     |    section       |
| padding |  adding-top | padding-bottom | padding-left | padding-right |
| select | margin-right | border-radius |  text area | font-family | 
| font-weight  | font-style |  line-height |  aside | position | 

---

