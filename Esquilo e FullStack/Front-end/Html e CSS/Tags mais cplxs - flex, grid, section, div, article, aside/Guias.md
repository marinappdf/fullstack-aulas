

# Aula de hoje

---

### Hoje vamos aprender a usar algumas tags mais elaboradas, ou entender melhor algumas que já conhecemos.
- div
- section
- grid
- flex
- article
- aside
- . . . 

---
### `<article>`

- **`<article>`:** conteúdo independente, pode ser reutilizado em outras páginas de um mesmo site -> autocontido, ou seja, possui um significado ou contexto próprio sem depender de outras seções da página

```
  <article>
	<footer>
		    <small>Publicado em <time datetime="2023-12-13">13 de Dezembro de 2023</time></small></footer>
  </article>
```
---
### `<aside>`
- **`<aside>`:** Utilizada para conteúdo relacionado, mas separado do conteúdo principal -> barra lateral, informações adicionais, anúncios

```markdown
  <aside>
    <h3>Artigos Relacionados</h3>
    <ul>
      <li><a href="#">Artigo 1</a></li>
      <li><a href="#">Artigo 2</a></li>
    </ul>
  </aside>
  ```
---
### `<header>`

- **`<header>`:** Define o cabeçalho de uma seção ou da página inteira. Pode incluir logotipos, menus de navegação, títulos, etc.

  ```markdown
  <header>
    <h1>Meu Site</h1>
    <nav>
      <ul>
        <li><a href="#">Início</a></li>
        <li><a href="#">Sobre</a></li>
        <!-- Mais itens do menu -->
      </ul>
    </nav>
  </header>
  ```
---
### `<footer>`
- **`<footer>`:** Define o rodapé de uma seção ou da página inteira. Pode conter informações de contato, direitos autorais, links úteis, etc.

  ```markdown
  <footer>
    <p>&copy; 2023 Meu Site</p>
    <p>Contato: contato@meusite.com</p>
  </footer>
  ```
---
### `<nav>`

- Usada para agrupar links de navegação principal ou secundário, ou seja, dentro da própria página

  ```markdown
  <nav>
    <ul>
      <li><a href="#">Início</a></li>
      <li><a href="#">Sobre</a></li>
      <li><a href="#">Contato</a></li>
      <!-- Mais itens do menu -->
    </ul>
  </nav>
  ```
---
### `<main>`

- Define o conteúdo principal da página.

  ```markdown
  <main>
    <h1>Bem-vindo ao Nosso Site!</h1>
    <p>Conteúdo principal da página...</p>
  </main>
  ```
---
### `<time>`

- Usada para representar datas e horas.

  ```markdown
  <p>Publicado em <time datetime="2023-12-13">13 de Dezembro de 2023</time></p>
  ```
---
### `<figure>`

- **`<figure>`:** Usada para incluir imagens ou gráficos, com ou sem legendas.

```markdown
  <figure>
    <img src="imagem.jpg" alt="Descrição da Imagem">
    <figcaption>Legenda da imagem</figcaption>
  </figure>
```
- Qual a diferença para img?

---
### Guia de uso da tag `<div>`:

1. **Criação de contêineres:** Use `<div>` para criar contêineres ou seções em sua página.

   ```html
   <div>
     <!-- Outros elementos aqui -->
   </div>
   ```

2. **Organização do conteúdo:** Utilize `<div>` para agrupar conjuntos de elementos relacionados.

   ```html
   <div>
     <h2>Título</h2>
     <p>Parágrafo explicativo.</p>
   </div>
   ```
---
#### . 

3. **Estilo com CSS:** Aplique estilos específicos usando classes ou IDs para identificar e estilizar as divs.

   ```html
   <div class="container">
     <!-- Conteúdo -->
   </div>
   ```

   ```css
   .container {
     background-color: blue;
     padding: 20px;
     border: 1px solid #ccc;
   }
   ```
---
###### .

4. **Layout e estruturação:** Use `<div>` para criar estruturas de layout.

   ```html
   <div class="header">
     <!-- Cabeçalho -->
   </div>
   <div class="main-content">
     <!-- Conteúdo principal -->
   </div>
   <div class="footer">
     <!-- Rodapé -->
   </div>
   ```
---
##### .

5. **JavaScript e interatividade:** Utilize `<div>` como contêineres para interações JavaScript ou manipulação dinâmica de conteúdo.

   ```html
   <div id="content" onclick="alterarConteudo()">
     <!-- Conteúdo dinâmico -->
   </div>
   ```

6. **Semântica:** Ao usar `<div>`, é importante manter a semântica adequada em seu código. Se houver um elemento HTML mais específico que se encaixe melhor no seu caso de uso (como `<header>`, `<section>`, `<article>`, etc.), prefira esses elementos semânticos.


---
### Guia de uso da tag `<section>`:

1. **Criação de seções:** Use `<section>` para definir seções distintas ou áreas temáticas em uma página.
	- Vamos ver que ele é bem parecido com div, mas que vai nos ajudar quando usado em conjunto com tags semânticas
   ```html
   <section>
     <!-- Conteúdo da seção -->
   </section>
   ```
---
##### . 
2. **Organização semântica:** Utilize `<section>` para separar e identificar conteúdo relacionado semanticamente.

   ```html
   <section>
     <h2>Introdução</h2>
     <p>Texto introdutório.</p>
   </section>
   ```

3. **Separação de conteúdo:** Use `<section>` para dividir diferentes partes do conteúdo da página.

   ```html
   <section>
     <!-- Conteúdo relacionado à equipe -->
   </section>
   <section>
     <!-- Conteúdo relacionado a depoimentos de clientes -->
   </section>
   ```
---
##### .

4. **Estilização e identificação:** Aplique estilos específicos usando classes ou IDs para identificar e estilizar as seções.

   ```html
   <section class="servicos">
     <!-- Conteúdo dos serviços -->
   </section>
   ```

   ```css
   .servicos {
     background-color: #f9f9f9;
     padding: 30px;
     border: 1px solid #ddd;
   }
   ```
---
##### .

5. **Hierarquia semântica:** Use `<section>` juntamente com outras tags semânticas (como `<header>`, `<article>`, `<footer>`, etc.) para criar uma estrutura semântica clara e compreensível. -> indicada quando o bloco criado é semântico, não é indicado para blocos de programação (tipo javascript)

   ```html
   <section>
     <header>
       <h2>Título da seção</h2>
     </header>
     <article>
       <!-- Conteúdo do artigo -->
     </article>
     <footer>
       <!-- Rodapé da seção -->
     </footer>
   </section>
   ```

- A tag `<section>` é usada para criar seções significativas em uma página web, organizando e estruturando o conteúdo de forma mais semântica e compreensível.

---
### Guia de uso do Flexbox:

1. **Ativação do Flexbox:** Defina um elemento pai como um container flexível usando `display: flex;` ou `display: inline-flex;`.

   ```css
   .container {
     display: flex;
     /* ou */
     display: inline-flex;
   }
   ```
---
##### . 

2. **Direção do eixo principal:** Defina a direção principal dos itens dentro do container flexível com `flex-direction`.

   ```css
   .container {
     flex-direction: row; /* (padrão) */
     /* ou */
     flex-direction: column;
   }
   ```

3. **Alinhamento dos itens:** Alinhe os itens ao longo do eixo principal e secundário com `justify-content` e `align-items`.

   ```css
   .container {
     justify-content: center; /* Alinhamento no eixo principal 
     ou */
     justify-content: space-between; /* Distribui o espaço igualmente entre os itens */
     align-items: flex-start; /* Alinhamento no eixo secundário */
   }
   ```
---
##### Itens!

4. **Tamanho flexível dos itens:** Use `flex` nos itens para definir como eles devem crescer ou encolher.

   ```css
   .item {
     flex: 1; /* Todos os itens têm o mesmo tamanho */
   }
   ```

5. **Alinhamento individual dos itens:** Use `align-self` para alinhar um item específico de forma diferente dos outros.

   ```css
   .item {
     align-self: flex-end; /* Alinha este item ao final do eixo secundário */
   }
```
---
### Como fica no código?
#### CSS

```css
	.container {
	  display: flex;
	}

	.column {
	  flex: 1; /* Ocupa o espaço disponível igualmente */
	  padding: 20px;
	  border: 1px solid #ccc;
	  margin: 5px;
}
```
#### HTML

```html
<div class="container">
  <div class="column">Coluna 1</div>
  <div class="column">Coluna 2</div>
  <div class="column">Coluna 3</div>
</div>

```
---

### Guia de uso do CSS Grid: responsivo e flexível

1. **Ativação do Grid:** Defina um elemento pai como um container grid usando `display: grid;`.

   ```css
   .container {
     display: grid;
   }
   ```

2. **Definição de linhas e colunas:** Configure as linhas e colunas do grid usando `grid-template-rows` e `grid-template-columns`.

   ```css
   .container {
     grid-template-columns: 100px 200px 100px; /* Três colunas de tamanhos específicos */
     grid-template-rows: 50px 100px; /* Duas linhas de tamanhos específicos */
     gap: 10px; /* Espaçamento entre linhas e colunas */
   }
```
---
##### .

4. **Posicionamento dos itens no grid:** Use `grid-column` e `grid-row` para posicionar itens específicos dentro do grid.

   ```css
   .item {
     grid-column: 2 / 4; /* Ocupa da coluna 2 até a coluna 4 */
     grid-row: 1 / span 2; /* Inicia na linha 1 e ocupa duas linhas */
   }
   ```

5. **Alinhamento dos itens:** Utilize `justify-items` e `align-items` para alinhar os itens dentro das células do grid.

   ```css
   .container {
     justify-items: center; /* Alinha os itens horizontalmente no centro das células */
     align-items: start; /* Alinha os itens verticalmente no início das células */
   }
```
---
##### .
6. **Responsividade com media queries:** Use media queries para ajustar o layout do grid para diferentes tamanhos de tela.

   ```css
   @media screen and (max-width: 768px) {
     .container {
       grid-template-columns: 1fr; /* Uma coluna para telas menores */
     }
   }
   ```
---
