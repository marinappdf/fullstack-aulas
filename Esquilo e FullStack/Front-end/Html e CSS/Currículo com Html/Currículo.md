
# Currículo
---
### Sugestões 

- **Organização Visual:**
  - Utilizar hierarquia visual (tamanhos de fonte, negrito, espaçamento) para destacar seções importantes.

- **Estilo Profissional:**
  - Utilizar uma paleta de cores coerente e fontes legíveis para criar uma aparência profissional.
---
### Comandos HTML Relevantes:

1. **Tags Semânticas:**
   - Utilizar tags semânticas como `<header>`, `<main>`, `<section>`, `<aside>`, `<footer>` para estruturar o currículo de forma organizada.

2. **Lista Ordenada e Não Ordenada:**
   - Utilizar `<ul>` e `<ol>` para listar habilidades, experiência, educação, etc.

3. **Links e Ancoragens:**
   - Usar `<a>` para adicionar links para redes sociais, portfólio online, e-mail, etc.

4. **Imagens:**
   - Usar `<img>` para adicionar uma foto pessoal ou logo.

5. **Tabelas:**
   - Utilizar `<table>` para organizar informações como histórico educacional ou experiência profissional.
---
### Propriedades CSS Relevantes:

1. **Estilização de Texto:**
   - `font-family`, `font-size`, `color`, `font-weight`, `text-align` para estilizar o texto.

2. **Espaçamento e Margens:**
   - `margin`, `padding` para controlar o espaçamento entre elementos.

3. **Background e Cores:**
   - `background-color`, `background-image` para adicionar cores de fundo ou imagens.

4. **Flexbox/Grid:**
   - Utilizar `flexbox` ou `grid` para criar layouts modernos e bem estruturados.

5. **Pseudo-classes e Pseudo-elementos:**
   - `:hover`, `:active`, `::before`, `::after` para adicionar efeitos interativos e decorativos.

6. **Layout Responsivo:**
   - Utilizar `media queries` para tornar o currículo responsivo em diferentes dispositivos.
---

## Sugestões para a estilização do texto

- `font-family` -> muda a fonte do texto
- `font-size` -> muda o tamanho do texto
	- Qual a diferença de usar `<h>`?
- `color`
- `font-weight` -> espessura!
- `text-align` -> alinhamento do texto

---
## **Espaçamento e Margens:**
   - `margin` -> espaço externo do objeto, ele em relação aos visinhos
	   - top, rigth, bottom, left
   - `padding` -> espaço interno entre o conteúdo do elemento e sua borda.

## **Background e Cores:**
   - `background-color`, `background-image` para adicionar cores de fundo ou imagens.
---

## Flex Box -> Modelo de layout unidimensional

```
.container {
  display: flex;
 justify-content: space-between;
}

.item {
 flex: 1;
 /* Outras propriedades de estilo */}
 ```
 ---

# exemplo

```
<div class="container">
	<div class="item">Item 1</div>
	<div class="item">Item 2</div>
	<div class="item">Item 3</div>
</div>
```


---
## Flex Box

1. **Propriedades no Container Flex:**
    
    - `display: flex;` ou `display: inline-flex;`: Define um elemento como um container flex.
    - `flex-direction`: Controla a direção principal dos itens (linha ou coluna).
    - `flex-wrap`: Determina se os itens devem quebrar em várias linhas se não couberem em uma única linha.
    - `justify-content`: Alinha os itens ao longo do eixo principal.
    - `align-items`: Alinha os itens ao longo do eixo secundário.
    - `align-content`: Controla o espaçamento entre as linhas quando há mais de uma linha de itens.
2. **Propriedades nos Itens Flex:**
    
    - `flex`: Determina como um item flexível irá crescer ou encolher em relação aos outros.
    - `order`: Especifica a ordem em que os itens são exibidos no container.
    - `align-self`: Permite sobrescrever o alinhamento definido pelo `align-items` para um item específico.

---

## Grid -> modelo de layout bidimensional

`.container {
 ` display: grid;
  `grid-template-columns: 100px 100px 100px; /* Três colunas de tamanho 100px cada*/
`  grid-template-rows: 50px 50px; /* Duas linhas de tamanho 50px cada */
 ` grid-gap: 10px; /* Espaçamento entre as linhas e colunas */
`}

`.item {
`  /* Propriedades de estilo */}

---

## exemplo

	- 
`<div class="container">
 ` <div class="item">Item 1</div>
  `<div class="item">Item 2</div>
  `<div class="item">Item 3</div>
`</div>




---

# **Pseudo-classes e Pseudo-elementos:**
   - `:hover`, `:active`, `::before`, `::after` para adicionar efeitos interativos e decorativos.

---
### Media queries

`@media` 
* CSS 
* criar layouts responsivos, adaptar páginas

`@media tipo-de-mídia e (condição) {   /* Estilos a serem aplicados para o tipo de mídia e condição específica */ }`

- `@media`: define conjuntos especiais para uma determinada mídia/tela
- `tipo-de-mídia`: Define o tipo de mídia para a qual os estilos se aplicarão.
	-  `screen` para telas de dispositivos -> geralmente para navegadores
	-  `print` para impressão
	- `speech` para sintetizadores de fala
- `condição`: Define as condições específicas que devem ser verdadeiras para que os estilos dentro da `@media` query sejam aplicados. As condições podem ser baseadas em largura de tela, altura de tela, orientação, densidade de pixels, etc.

---

### Exemplo

```
/* Estilos para telas maiores */
h1 {
  font-size: 36px; } 
p {   
line-height: 1.5; }  
/* Estilos para telas menores */
@media screen and (max-width: 768px) {
   h1 { 
    font-size: 24px;   }  
  p { 
    line-height: 1.3;   } }`

```

---




---