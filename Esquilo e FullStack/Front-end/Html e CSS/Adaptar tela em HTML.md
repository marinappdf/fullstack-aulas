

# Sites responsívos


### **Unidades Relativas:**

Porcentagens, `em`, `rem`, e `vw` (viewport width) para definir tamanhos e margens em relação ao tamanho da tela:

#### Porcentagem

- **% (Porcentagem)**: Essa unidade define um valor em relação ao valor do elemento pai.

```css
.container {
  width: 80%; /* Define a largura do container como 80% da largura do elemento pai */
}
```
#### Em

- **em**: Esta unidade é baseada no tamanho da fonte do elemento pai. Ela pode ser útil para definir tamanhos de fonte relativos.

```css
body {
  font-size: 16px; /* Define o tamanho da fonte base do corpo do documento */
}

h1 {
  font-size: 2em; /* Tamanho da fonte será o dobro da fonte base (32px) */
}

```

#### REM

- **rem**: Similar ao `em`, mas ao invés de se basear no elemento pai, ele se baseia no tamanho da fonte do elemento raiz (normalmente o `<html>`).

```css
html {
  font-size: 16px; /* Define o tamanho de fonte base */
}

.container {
  font-size: 1.2rem; /* Aumenta o tamanho da fonte em 20% em relação à fonte base (19.2px) */
}
```

#### Viewpoint

- **vw, vh**: Essas unidades são baseadas no tamanho da janela do navegador. `vw` representa a largura da janela, enquanto `vh` representa a altura da janela.

```css
.section {
  width: 80vw; /* Define a largura da seção como 80% da largura da janela */
  height: 50vh; /* Define a altura da seção como 50% da altura da janela */
}
```

#### **Media Queries:** 
- Utilize media queries para aplicar estilos específicos dependendo do tamanho da tela. Isso permite adaptar o layout e o design para diferentes dispositivos.

   ```css
   @media screen and (max-width: 768px) {
     /* Estilos para telas menores do que 768px, como smartphones 
     Quando o tamanho máximo da tela for 768px, aplique essas configurações.*/
     font-size: 14px;
   }
   @media screen and (min-width: 769px) and (max-width: 1024px) {
     /* Estilos para telas médias, como tablets 
     Quando a tela tiver entre essas dimensões, aplique essas configurações:*/
     font-size:20px;
   }
   @media screen and (min-width: 1025px) {
     /* Estilos para telas maiores, como desktops 
     Quando a tela for maior do que esse tamanho, aplique essa configuração*/
     font-size: 30px;
   }
   ```

#### **Media Queries

```css
@media tipo-de-midia e (condição) {
  /* Estilos a serem aplicados para essa condição */
}
```
- **`@media`**: Indica que se trata de uma regra de Media Query.
- **`tipo-de-midia`**: Define o tipo de mídia, como `screen` para telas de computador, `print` para impressão, `all` para todos os dispositivos, . . .
- **`condição`**: Determina a condição que deve ser verdadeira para aplicar os estilos. Pode ser baseada em largura (`max-width`, `min-width`), altura, orientação (`landscape`, `portrait`), densidade de pixels (`min-resolution`), entre outras.
**Importante:** Quando formos utilizar media queries, o primeiro passo é adicionar uma metatag chamada viewport no do site. Essa tag vai passar instruções para o browser renderizar o conteúdo do site conforme o tamanho do dispositivo.
### **Imagens Responsivas:** 

Utilize a propriedade `max-width: 100%;` em imagens para garantir que elas se dimensionem corretamente em telas menores sem distorção.


### **Grid Systems e Flexbox:**

Utilize sistemas de grid ou Flexbox para criar layouts flexíveis que se ajustem dinamicamente a diferentes tamanhos de tela.

### **Viewport Meta Tag:** 

Use a tag `<meta>` com `viewport` para controlar o dimensionamento e a escala inicial do site em dispositivos móveis:

   ```html
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   ```
https://www.youtube.com/watch?v=g__c-7M9Xzk&t=94s

vh e vw

Muitas técnicas de web design responsivo dependem muito de regras percentuais. **Mas e se fosse preciso usar a largura ou a altura da viewport ao invés da largura do elemento-pai?** Isso é exatamente o que as unidades vh e vw proporcionam.

A medida vh é igual a **1/100** da altura da viewport. Então, por exemplo, se a altura do navegador é 900px, 1vh equivale a 9px e, analogamente, se a largura da viewport é 750px, 1vw equivale a 7.5px. Sendo assim, **1vw = 1% da largura da viewport e 1vh = 1% da altura da viewport**.
###  **Testes, Ajustes e combinações:** 

1. Faça testes em diferentes dispositivos
2.  **Teste Localmente:** redimensione a janela para ver como o layout responde a diferentes larguras, ou:
3. Ferramentas de desenvolvedor do navegador
	2. **Google Chrome:** Abra o arquivo HTML localmente no Chrome e pressione F12 para abrir as ferramentas de desenvolvedor. Clique no ícone de dispositivo (ícone de celular/tablet) no canto superior esquerdo das ferramentas de desenvolvedor para simular diferentes dispositivos ou ajustar a largura e altura da janela para ver como o layout se comporta.
4. **Teste em Dispositivos Reais:**
5. **Teste em Ferramentas Online:** 
	1. http://www.responsinator.com/
	2. [Responsinator - wikipedia.org](http://www.responsinator.com/?url=wikipedia.org)

## Atividade de hoje:

1. Testar a responsividade de pelo menos 5 sites no http://www.responsinator.com/
2. Criar um site simples com responsividade usando unidades relativas. Tente usar todas elas, ou seja, 4 versões do mesmo site
	2. Coloque um cabeçalho (header), um rodapé (footer), um pequeno texto principal (main) e uma imagem.
	3. Primeiro, defina o max-width de cada um desses blocos (div) como um valor fixo em px, defina o tamanho da fonte de cada texto em pt. Abra o site e analise a resposta aos diferentes tamanhos (F12)
	4. Em porcentual:
		1.  Defina o max-width dos blocos como 100%, analise o resultado
		2. Defina o max-width da imagem como 100%, analise o resultado.
	5. Em rem: 
		1. Defina o tamanho da fonte em pt na raiz do progrma (body)
		2.  Mude as fontes para um valor em rem relativo ao da raiz.
		3. 
	6. Em em
		1. 
	7. Em viewport
		1. 
	
1. Criar outro site simples com repsonsividade usando mediaquery [[media-query-exemplo.html]]
2. Criar um site simples usando flex box e testar sua responsividade: [[2colunas-ajustável.html]]
3. Criar um site usando grid e testar sua responsividade

---

REFs
https://github.com/reprograma/On10-TodasEmTech-Responsividade