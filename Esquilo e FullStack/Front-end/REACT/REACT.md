
# Aula 1 - Introduzindo tema

## Introdução

Sobre: https://react.dev/learn

[React: desenvolvendo em React Router com JavaScript: Aula 1 - Atividade 1 Apresentação | Alura - Cursos online de tecnologia](https://cursos.alura.com.br/course/React-desenvolvendo-react-router-javaScript/task/112257)

React é uma biblioteca com componentes que são funções de JavaScript.

Esses componentes são partes das Interfaces de Usuário e retornam resultados de markup (acho que isso significa em marcação de texto).

---

## Algumas anotações:

- Para entender React, é preciso entender javascript antes
- Vocês já viram um pouco disso antes de eu chegar, mas vou começar "do início" para que todes acompanhem.
- As funções em React DEVEM sempre começar com a letra maiúscula
- tags de html devem sempre estar em letra minúscula



---


## Material para ver em casa

- Para assistir: https://www.hipsters.tech/do-front-end-ao-react-hipsters-ponto-tech-258/
- Para ler sobre o NodeJS: https://www.alura.com.br/artigos/nodejs-para-frameworks-front-end?_gl=1*2lmxr1*_ga*MTg4ODYyMTU3LjE3MDMxNjMxNjU.*_ga_1EPWSW3PCS*MTcwNTUxNjY0NS4yMi4xLjE3MDU1MTY5MjIuMC4wLjA.*_fplc*ZGNrTXIxZkg2Y1ElMkYlMkJzWXc1bHVLWkdpWXcyM2VLWnJHall0WU1CZzVNN0Y3QjhnemhBdjFVQWZ2UERDZ3pESWJxaFU1U0FiRVBoZXZRNG0xaVo5OUZxYm0lMkJFQ3VReXhvN1lFbURFd0ZPYmxMS1BnWG1ISmhlUmZ0SkhvcWhnJTNEJTNE
---



## Vamos fazer o curso "Explore React com JavaScript"

"Aprenda a criar interfaces do usuário com React, um dos frameworks JavaScript mais utilizados, e desenvolva uma aplicação do zero ao deploy"

Disponível na alura: https://cursos.alura.com.br/formacao-react-javascript


## O primeiro curso é: React: desenvolvendo com JavaScript

https://cursos.alura.com.br/course/react-desenvolvendo-javascript

Mas podemos pular o vídeo de apresentação e come



---

## Corrigindo nosso Node.js

- Vamos corrigir nossa instalação.
- Primeiro passo: desinstalar
	- Vá em downloads
	- Clique duas vezes no arquivo do node
	- Marque a opção de remover o arquivo
- Segundo passo:
	- Clique duas vezes
	- Execute o programa 
	- Instale novamente
- **ATENÇÃO!! QUEREMOS MARCAR A CAIXA QUE DIZ PARA QUE SEJAM ISNTALADAS TODAS AS DEPENDÊNCIAS ENCESSÁRIAS!!!
- Feito isso, vamos instalar o React:
```lua
npx create-react-app organo
```
- Tive problemas com uma pasta específica, tive que ir criar ela na mão! Não sei se deu certo, mas o programa foi instalado.
- Instalando outros pacotes: https://pt-br.react.dev/learn/start-a-new-react-project
 
---



---

---
# Aula 2 



## Revisão

Rever aula: # 03 Olá mundo!

https://cursos.alura.com.br/course/react-desenvolvendo-javascript/task/107393


npm:  rodar local
npx: rodar remoto

 C:\Users\Tarde\organo


---

## Introdução ao REACT-JSX

 01 Uma biblioteca declarativa - Primeiro componente banner


---

# Aula 3 - 22/01

## Recordar é viver

- O que fizemos aula passada?

	- Começou a usar o ract
	- Muitos problemas técnicos
	- Maioria conseguiu realizar as alterações que a gente queria
	- Caminho
		- cmd (prompt de comando) 
		- cd organo (entrar na pasta onde o projeto está) 
		- npm start





assistimos um tanto, fomo até o início do vídeo 8 (acho)
força guerreiros para conseguirmos terminar.

Talvez eu tente a estratégia de enviar os pdfs para cada um e daí vamos lendo e fazendo.
eu posso fazer ao mesmo tempo, eu vou terminar antes e posso explicar. a ordem seria
1 - Enviar os pdfs
2 - Fazer as tarefas do pdf 1, eles e eu
3 - explicar o pdf 1 e 2
4 - passar de mesa em mesa tentando ajudar
5 - quando todos concluirem, eu faço o próximo pdf
6 - etc. . . .. 
















## Conteúdo expositivo
 
 01 Uma biblioteca declarativa - 05 Explorando o projeto - https://cursos.alura.com.br/course/react-desenvolvendo-javascript/task/107395

Finalizado










----

# Aula 4 - 23/01 


### React: desenvolvendo ocm javascript - 02 Trabalhando com props

Link da aula: https://cursos.alura.com.br/course/react-desenvolvendo-javascript/task/107542

Link do projeto da aula anterior:
- https://github.com/alura-cursos/organo/tree/aula-1
- https://github.com/alura-cursos/organo/archive/refs/heads/aula-1.zip

- Depois de baixar o zip no discord
- E copiar o código atualizado (para quem quiser)
- Abre o cmd, ou prompt de comando
	- Na linha comando, digite
	- `cd organo`
	- `npm start`








# Aula 5 - 24/01

## Recordar é Viver

- Boa tarde
- Relembrar sobre os o prazo de 48h para envio de atestados
- Sobre o código de ontem, achei o erro! É necessário definir 'evento' como parâmetro da função!!!

```js
 const aoSalvar = (evento) => {

        evento.preventDefault()

        console.log('Form foi submetido')

    }
```
- Erros de importação: configurar o auto-save, ou fechar as abas e salvar os arquivos. Temos algum problema na comunicação do VScode com o React
- Sobre a aula de hoje, vou enviar no tópico o zip do código de ontem e os PDFS!
	- Link para os códigos da aula: https://github.com/alura-cursos/organo/tree/aula-2

## Conteúdo expositivo: 03. Interagindo com o Usuário

https://cursos.alura.com.br/course/react-desenvolvendo-javascript/task/107547

Tentar adiantar parte da aula 4

# Aula 6 - 25/01: Finalizando o REACT + Orientação Individual 

## Finalizando o REACT

Projeto Antes do Debug: https://github.com/alura-cursos/organo/tree/aula-4



[Caça aos BUGS - React: desenvolvendo com JavaScript: Aula 5 - Atividade 1 Projeto da aula anterior | Alura - Cursos online de tecnologia](https://cursos.alura.com.br/course/react-desenvolvendo-javascript/task/107557)

Projeto final: https://github.com/alura-cursos/organo/tree/aula-5

## Orientação Individual

Para assistir durante a orientação individual:
 Uso de IA para melhorar a produtividade:
 https://cursos.alura.com.br/meu-plano-de-estudos-marina-freitas4-1706127522391-p717087
 


# Aula N - Futuro - JSX


JSX

https://cursos.alura.com.br/course/react-construindo-componentes-jsx