


# DOM

#javascript   #DOM #alura

https://javascript.info/dom-nodes

---

# Aula 1

[Projeto Fokus – Figma](https://www.figma.com/file/dEaMv34Wd5G7TBMPo8fPlK/Projeto-Fokus?type=design&node-id=35-181&mode=design)


[GitHub - alura-cursos/Fokus at projeto-base](https://github.com/alura-cursos/Fokus/tree/projeto-base)

[React: desenvolvendo em React Router com JavaScript: Aula 1 - Atividade 1 Apresentação | Alura - Cursos online de tecnologia](https://cursos.alura.com.br/course/React-desenvolvendo-react-router-javaScript/task/112257)






https://www.freecodecamp.org/news/what-is-the-dom-document-object-model-meaning-in-javascript/

querySelector lê todo o arquivo e provura essa tag

document.querySelector('button')
<button data-contexto=​"foco" class=​"app__card-button app__card-button--foco active">​Foco​</button>​
document.querySelectorAll('button')


seleciona todos os botões do codigo


document.querySelector('button')
<button data-contexto=​"foco" class=​"app__card-button app__card-button--foco active">​Foco​</button>​
document.querySelectorAll('button')



document.getElementsByClassName('app__card-button--curto')



t


document.getElementsById('start-pause')




odo `document.getElementsByID('botão')`


`document.querySelector('botão')`

o `document.getElementByTagName('botão')`

 `document.querySelectorAll('button')`


<script src="./script.js" defer></script>
// atrasa a leitura do site, isso acontece para nao atrasar a leitura do site, para nao travar, quando coloco o script no head precisa disso
quando coloco no body nao precisa 

----


Para entendermos bem sobre este assunto, primeiro precisamos esclarecer uma coisa: O que é DOM? DOM é a sigla para Document Object Model (Modelo de Objeto de Documento) e é uma forma padronizada de representar e interagir com objetos em documentos HTML e XML. O DOM representa a estrutura de um documento através de uma árvore de objetos, onde cada objeto representa uma parte do documento.

Entender essa definição ajuda a compreender a importância dos eventos e de como eles funcionam no DOM (Document Object Model), sendo essencial para avançar seus estudos em JavaScript.

**O que é um Evento no DOM?**

Pense nisso como sinais que o navegador envia quando algo acontece - como um usuário que clica em um botão em sua página web, por exemplo. Quando um evento ocorre, você tem a capacidade de reagir a ele e executar algumas ações, como exibir uma mensagem para o usuário, alterar ou adicionar algum elemento na página.

### **O Método `addEventListener`**

Antes de mergulharmos nos diferentes tipos de eventos, vamos entender rapidamente como o método addEventListener funciona. Ele é um método disponível para todos os elementos HTML e permite que registremos funções (callbacks) que serão chamadas quando um evento específico ocorrer.

A sintaxe básica é a seguinte:

`elemento.addEventListener(evento, callback);`

Onde:

- elemento: É o elemento HTML ao qual queremos associar o evento.
- evento: É uma string que representa o tipo de evento que desejamos capturar.
- callback: É a função que será chamada quando o evento ocorrer.

### **Tipos de Eventos**

- _Eventos de clique (click)_

Os eventos de clique são alguns dos mais utilizados em desenvolvimento web. Eles ocorrem quando o usuário clica em um elemento específico da página, como um botão, um link ou até mesmo em uma imagem. Podemos usar o evento click para executar ações quando o usuário interage com esses elementos.

Exemplo:

```javascript
// HTML <button id="meuBotao">Clique aqui</button>

const meuBotao = document.getElementById("meuBotao");
meuBotao.addEventListener("click", function() {
  alert("O botão foi clicado!");
});
```

Quando o usuário clicar no botão com o texto "Clique aqui", um alerta será exibido com a mensagem "O botão foi clicado!".

- _Eventos de submissão de formulário (submit)_

Quando temos um formulário em nossa página, podemos usar o evento submit para capturar a submissão do formulário pelo usuário. Isso nos permite executar ações como validar os dados inseridos antes de enviá-los para o servidor.

Exemplo:

```bash
//HTML <form id="meuFormulario">
//HTML     <input type="text" name="nome" />
//HTML     <input type="submit" value="Enviar" />
//HTML  </form>

const meuFormulario = document.getElementById("meuFormulario");
meuFormulario.addEventListener("submit", function(event) {
  event.preventDefault(); // Impede o envio padrão do formulário
  const nome = event.target.elements.nome.value;
  alert(`O formulário foi enviado com o nome: ${nome}`);
});
```

Quando o usuário preencher o campo de texto do formulário e clicar no botão "Enviar", um alerta será exibido com a mensagem "O formulário foi enviado com o nome: [nome inserido no campo]".

- _Eventos de teclado (keydown, keyup, keypress)_

Os eventos de teclado permitem que respondamos às ações do usuário no teclado, como pressionar ou soltar uma tecla específica. Existem três principais tipos de eventos de teclado:

_keydown:_ Ocorre quando uma tecla é pressionada. _keyup:_ Ocorre quando uma tecla é solta. _keypress:_ Ocorre quando uma tecla é pressionada e ainda não foi solta.

Exemplo:

```javascript
//HTML<input type="text" id="meuInput" />

const meuInput = document.getElementById("meuInput");
meuInput.addEventListener("keydown", function(event) {
  console.log(`Tecla pressionada: ${event.key}`);
});
```

Quando o usuário pressiona uma tecla enquanto o cursor está no campo de texto, o evento keydown será acionado e o código imprimirá no console a mensagem "Tecla pressionada: [tecla pressionada]".

- Eventos de foco (focus, blur)

Os eventos de foco são usados quando queremos capturar quando um elemento recebe ou perde o foco. O evento focus ocorre quando o elemento ganha o foco (por exemplo, quando clicamos em um campo de formulário), enquanto o evento blur ocorre quando o elemento perde o foco.

Exemplo:

```lua
//HTML <input type="text" id="meuCampo" />

const meuCampo = document.getElementById("meuCampo");
meuCampo.addEventListener("focus", function() {
  console.log("Campo ganhou o foco.");
});

meuCampo.addEventListener("blur", function() {
  console.log("Campo perdeu o foco.");
});
```

Quando o usuário clicar no campo de texto, o evento focus será acionado e o código imprimirá no console a mensagem "Campo ganhou o foco.". Quando o usuário clicar fora do campo, fazendo-o perder o foco, o evento blur será acionado e o código imprimirá no console a mensagem "Campo perdeu o foco.".

O método addEventListener em JavaScript é uma poderosa ferramenta para lidar com eventos em elementos HTML. Através dos diferentes tipos de eventos disponíveis, podemos criar páginas web mais interativas e responsivas, melhorando a experiência do usuário.

Abordamos os eventos mais comuns, e também existem muitos outros que podem ser explorados para atender às necessidades específicas de cada projeto. Portanto, o conhecimento sobre eventos e a habilidade de utilizá-los adequadamente são fundamentais para se tornar uma pessoa desenvolvedora web mais eficiente e versátil.


---



---


---

# Aula 2


## Recordar é viver: 

Vamos começar então revendo o que foi feito na aula anterior.

- O que vocês lembram da aula de sexta?
- Me digam 3 coisas que vocês aprenderam.



- Alteramos o fundo e a imagem do projeto Fokus de um carioca

### O que cada um desses comandos faz?

- ` document.querySelector('button')`
- `document.querySelectorAll('button')
- `document.getElementsByClassName('app__card-button--curto')`
- `document.getElementsById('start-pause')`
- `<script src="./script.js" defer></script>`



## Sabendo mais sobre os atributos

![[getAttribute, setAttribute, hasAttribute e removeAttribute..pdf]]




## Alterando o Texto


- Identifiquem no HTML onde está o texto. Vamos brincar de alterar e ver o que acontece.

-  Se quisermos alterar conforme o clique do botão, como poderíamos fazer isso?

###  Tutorial: 
 
 Ache de forma automática onde está o texto no HTML, e jogue numa variável.

> _`script.js`:_

```js
const titulo = document.querySelector('.app__title')
```
- O endereço das tags com class .app_title serão colocadas na variável titulo.
- O `innerHTML` tem o poder de fazer uma inserção dentro do html, no caso, em todos os endereços salvos em titulo.

```js
 titulo.innerHTML = 'Otimize sua produtividade, mergulhe no que importa'
```

ou 

```html
titulo.innerHTML = ` Otimize sua produtividade,<br>
<strong class="app__title-strong">mergulhe no que importa.</strong>`

```
- Essa última traz a formatação do texto. As demais frases encontram-se também no código




### Vamos criar uma função que altere o site conforme o clique. Uma função única.


Uma função teria uma cara maios ou menos assim:
```js
function alterarContexto(contexto) {
    html.setAttribute('data-contexto', contexto)
    banner.setAttribute('src', `/imagens/${contexto}.png`)
 // . . . 
    }
}
```
Onde contexto é o parâmetro de entrada. Para isso funcionar, o nome da class e da imagem tem que ser o mesmo. Por isso foco e foco.png.

Para automatizar a alteração do texto, precisamos de de uma análise da condicional mais complexo.  A ideia é que o que estará escrito na página muda com o valor da chave contexto:
	- Se a chave contexto tem o valor igual a foco, o texto na página deve ser "abcd".
	- Se a chave contexto tem o valor igual a descanso-curto, o texto na página deve ser "efgh".
	- Se a chave contexto tem o valor igual a descanso-longo, o texto na pagina deve ser "ifg".


Para fazer isso, vamos aprender sobre um comando novo que funciona tipo um IF, é uma condicional `switch

```js
   switch (chave) {
        case valor1:
   			// resto do código
            break;
	    case valor2:
			// resto do código
			break;
	    case valor3:
			// resto do código
			break;


```

Pesquise como esse comando funciona antes de continuar.


Então, eu quero algo mais ou menos assim

```js
   switch (contexto) {
        case 'foco':
   			// resto do código
            break;
	    case 'descanso-curto':
			// resto do código
			break;
	    case 'descanso-longo':
			// resto do código
			break;

```

 E como vamos mudar esse texto?
- Dentro desse primeiro `case`, chamaremos a variável `titulo` que acabamos de criar. 
- Para inserir um texto na página, usaremos um novo método para manipular elementos no DOM, específico para trabalhar com textos no HTML: o 
	 `innerHTML`.
- Lembrando que: 
```js
const titulo = document.querySelector('.app__title')
```
### Agora vocês, considerando isso, como ficariam as próximas cases?

- Identifique no HTML os textos referentes a cada botão (foco, descanso-curto e descanso-lomgo)
- escreva como ficariam as cases em cada caso
- Implemente no código completo e veja o resultado.



## Resposta, só spoilers, please


```js
function alterarContexto(contexto) {
    html.setAttribute('data-contexto', contexto)
    banner.setAttribute('src', `/imagens/${contexto}.png`)
    switch (contexto) {
        case "foco":
            titulo.innerHTML = `
            Otimize sua produtividade,<br>
                <strong class="app__title-strong">mergulhe no que importa.</strong>
            `
            break;
        case "descanso-curto":
            titulo.innerHTML = `
            Que tal dar uma respirada? <strong class="app__title-strong">Faça uma pausa curta!</strong>
            ` 
            break;
        case "descanso-longo":
            titulo.innerHTML = `
            Hora de voltar à superfície.<strong class="app__title-strong"> Faça uma pausa longa.</strong>
            `
        default:
            break;
    }
}
```



#### Referência para criando uma função:
https://cursos.alura.com.br/course/javascript-manipulando-elementos-dom/task/135950



---

# Aula 3 - 16/01


## Reflexão e introdução 

- [x]  Enviar para elas o projeto da aula anterior

[JavaScript: manipulando elementos no DOM: Aula 3 - Atividade 1 Projeto da aula anterior | Alura - Cursos online de tecnologia](https://cursos.alura.com.br/course/javascript-manipulando-elementos-dom/task/136962)





---
## Recordar é Viver

- [x] Revisar o projeto da aula anterior: Está na pasta Fokus-aula-2-aula16-01, dentro do projeto Fokus. 

- O que está acontecendo nesse código?
```js
 function alterarContexto(contexto) {
    html.setAttribute('data-contexto', contexto)
    banner.setAttribute('src', `/imagens/${contexto}.png`) 
    }

alterarContexto("descanso-curto")
```





- O que faz o comando `switch`?
```js
	switch (parâmetro) {
		case valor1:
			//código
			break;
		case valor2:
			//código
			break;
			}
```

- O que o switch está fazendo nesse código? Porque precisamos dele?
 ```js
 function alterarContexto(contexto) {
    html.setAttribute('data-contexto', contexto)
    banner.setAttribute('src', `/imagens/${contexto}.png`)
    switch (contexto) {
        case "foco":
            titulo.innerHTML = `
            Otimize sua produtividade,<br>
                <strong class="app__title-strong">mergulhe no que importa.</strong>
            `
            break;
        case "descanso-curto":
            titulo.innerHTML = `
            Que tal dar uma respirada? <strong class="app__title-strong">Faça uma pausa curta!</strong>
            ` 
            break;
        case "descanso-longo":
            titulo.innerHTML = `
            Hora de voltar à superfície.<strong class="app__title-strong"> Faça uma pausa longa.</strong>
            `
        default:
            break;
    }
```


---


## Aplicando áudio e som
https://cursos.alura.com.br/course/javascript-manipulando-elementos-dom/task/136962

- Após cada vídeo, vamos parar para fazermos  a tarefa



---
# Aula 4 -  NÃO REALIZADA

https://cursos.alura.com.br/course/javascript-manipulando-elementos-dom/task/135954





https://cursos.alura.com.br/course/javascript-manipulando-elementos-dom/task/135956

---

# Anotações sobre DOM

- De acordo com o Document Object Model (DOM), toda tag HTML é um **objeto**.
 - Tags aninhadas são “filhas” daquela que as contém. 
 - O texto dentro de uma tag também é um objeto.

Ou seja, o DOM transforma partes do HTML em objetos relacionados entre si. Essa relação é feita no modelo de árvore. Cada TAG é o topo de um árvore, ou sua raiz, e as tags subsequantes são suas filhas, suas ramificações. O texto dentro delas é também lido como uma tag, uma ramificação.

Com o JavaScript podemos acessar esses itens. Ou seja, com o Javascript conseguimos modificar a página. É isso que permite que a página seja dinâmica, e não estatática. Essa dinâmica significa que os elementos html estão sendo modificados (pelo javascript).

O primeiro elemento é o `document`, que representa o documento que estamos trabalhondo, o documento HTML que chamar o script.js.

Ele pode ser seguido da tag sobre a qual vamos atuar, a própria classe. Por exemplo, `<body>`. Então, `document.body` atua sobre o `<body>` do documento. Ambos são O B J E T O S!
Mais quais são as propriedades desses objetos? São os seus atributos. Por exemplo, o estilo e cor de fundo são propriedades possíveis. Então chamamos os atributos `style.background`.
Bom, sabendo quem são nossos objetos e seus stributos, temos que decidir como vamos atuar sobre eles, quais ações eles permitem. Ou seja, os métodos associados a esses objetos.

Um exemplo de método é o `setTimeout()`. Como parâmetro, podemos colocar a ação que será feita (variável que será alterada) depois de um determinado tempo.
Assim, `setTimeout(() => document.body.style.background = '', 3000)`

Outras propriedades que existem são:
- `innerHTML`– Conteúdo HTML do nó.
- `offsetWidth`– a largura do nó (em pixels)

Aqui podemos ver uma imagem que representa a estrutura de árvores do HTML. Cada divisião de linha é um nó, onde tem as setinhas. Cada nó abre uma tag mãe para seus filhos.
Por exemplo, HTML é mãe de HEAD, tex e do Body.
![[Pasted image 20240117143616.png]]

Para ver a estrutura do DOM em tempo real, experimente [o Live DOM Viewer](https://software.hixie.ch/utilities/js/live-dom-viewer/) . Basta digitar o documento e ele aparecerá como DOM instantaneamente.

Outra forma de explorar o DOM é usar as ferramentas de desenvolvedor do navegador. Na verdade, é isso que usamos no desenvolvimento.

Para fazer isso, abra a página web [elk.html](https://javascript.info/article/dom-nodes/elk.html) , ative as ferramentas de desenvolvedor do navegador e mude para a guia Elementos.

* No topo: documentElement e body

Os nós da árvore superiores estão disponíveis diretamente como documentpropriedades:

`<html>=document.documentElement`
O nó do documento mais alto é `document.documentElement`. Esse é o nó DOM da `<html>`tag.
`<body>=document.body`
Outro nó DOM amplamente utilizado é o `<body>`elemento – `document.body`.
`<head>=document.head`
A `<head`>tag está disponível como `document.head`.

## Navegando pelos elementos DOM

- Para todos os nós: `parentNode`, `childNodes`, `firstChild`, `lastChild`, `previousSibling`, `nextSibling`.
- Apenas para nós de elemento: `parentElement`, `children`, `firstElementChild`, `lastElementChild`, `previousElementSibling`, `nextElementSibling`.
### Pesquisando: getElement*, querySelector*

Para obter um elemento arbitrário da página!
https://javascript.info/searching-elements-dom

Se um elemento possui o `id`atributo, podemos obter o elemento usando o método `document.getElementById(id)`, não importa onde ele esteja

O `id`deve ser único

O `id`deve ser único. Pode haver apenas um elemento no documento com o arquivo `id`.

Se houver vários elementos com o mesmo `id`, então o comportamento dos métodos que o utilizam é ​​imprevisível, por exemplo, `document.getElementById`pode retornar qualquer um desses elementos aleatoriamente. Portanto, siga a regra e mantenha-se `id`único.

`elem.querySelectorAll(css)`retorna todos os elementos `elem`que correspondem ao seletor CSS fornecido.

Pseudoclasses no seletor CSS como `:hover`e `:active`também são suportadas. Por exemplo, `document.querySelectorAll(':hover')`retornará a coleção com os elementos sobre os quais o ponteiro está agora (na ordem de aninhamento: do mais externo `<html>`para o mais aninhado).


`getElmentBy`: Retorna uma coleção, não um elemento! 

- `elem.getElementsByTagName(tag)`procura elementos com a tag fornecida e retorna a coleção deles. O `tag`parâmetro também pode ser uma estrela `"*"`para “qualquer tag”.
- `elem.getElementsByClassName(className)`retorna elementos que possuem a classe CSS fornecida.
- `document.getElementsByName(name)`retorna elementos com o `name`atributo fornecido, em todo o documento. Muito raramente usado.



|Método|Pesquisas por...|Pode chamar um elemento?|Ao vivo?|
|---|---|---|---|
|`querySelector`|Seletor CSS|✔|-|
|`querySelectorAll`|Seletor CSS|✔|-|
|`getElementById`|`id`|-|-|
|`getElementsByName`|`name`|-|✔|
|`getElementsByTagName`|etiqueta ou`'*'`|✔|✔|
|`getElementsByClassName`|aula|✔|✔|

De longe, os mais usados ​​são `querySelector`e `querySelectorAll`, mas `getElement(s)By*`podem ser esporadicamente úteis ou encontrados em scripts antigos.

Além disso:

- É `elem.matches(css)`necessário verificar se `elem`corresponde ao seletor CSS fornecido.
- É `elem.closest(css)`necessário procurar o ancestral mais próximo que corresponda ao seletor CSS fornecido. O `elem`próprio também é verificado.

E vamos mencionar mais um método aqui para verificar o relacionamento entre pais e filhos, pois às vezes é útil:

- `elemA.contains(elemB)`retorna verdadeiro se `elemB`estiver dentro `elemA`(um descendente de `elemA`) ou quando `elemA==elemB`.

## Propriedades do nó: tipo, tag e conteúdo

https://javascript.info/basic-dom-node-properties

**Como podemos ver, os nós DOM são objetos JavaScript regulares. Eles usam classes baseadas em protótipos para herança.**


A propriedade [innerHTML](https://w3c.github.io/DOM-Parsing/#the-innerhtml-mixin) permite obter o HTML dentro do elemento como uma string.
- O que está dentro de uma tag, ede um elmento, é capturado e retornado como uma string.
Também podemos modificá-lo. Portanto, é uma das maneiras mais poderosas de mudar a página.

Cada nó DOM pertence a uma determinada classe. As classes formam uma hierarquia. O conjunto completo de propriedades e métodos é resultado de herança.

As principais propriedades do nó DOM são:

`nodeType`

Podemos usá-lo para ver se um nó é um nó de texto ou de elemento. Possui um valor numérico: `1`para elementos, `3`para nós de texto e alguns outros para outros tipos de nós. Somente leitura.

`nodeName/tagName`

Para elementos, nome da tag (em maiúsculas, exceto no modo XML). Para nós não elementares `nodeName`descreve o que é. Somente leitura.

`innerHTML`

O conteúdo HTML do elemento. Pode ser modificado.

`outerHTML`

O HTML completo do elemento. Uma operação de gravação `elem.outerHTML`não `elem`se altera. Em vez disso, ele é substituído pelo novo HTML no contexto externo.

`nodeValue/data`

O conteúdo de um nó não elementar (texto, comentário). Esses dois são quase iguais, geralmente usamos `data`. Pode ser modificado.

`textContent`

O texto dentro do elemento: HTML menos tudo `<tags>`. Escrever nele coloca o texto dentro do elemento, com todos os caracteres especiais e tags tratados exatamente como texto. Pode inserir com segurança texto gerado pelo usuário e proteger contra inserções indesejadas de HTML.

`hidden`

Quando definido como `true`, faz o mesmo que CSS `display:none`.

Os nós DOM também possuem outras propriedades dependendo de sua classe. Por exemplo, `<input>`elementos ( `HTMLInputElement`) suportam `value`,, `type`enquanto `<a>`elementos ( `HTMLAnchorElement`) suportam `href`etc. A maioria dos atributos HTML padrão tem uma propriedade DOM correspondente.

Entretanto, os atributos HTML e as propriedades DOM nem sempre são iguais, como veremos no próximo capítulo.

## Atributos e propriedades do DOM

. . . 