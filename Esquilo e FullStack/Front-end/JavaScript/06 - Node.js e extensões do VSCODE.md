# Node.js

Referência: [Node.JS: definição, características, vantagens e usos possíveis | Alura](https://www.alura.com.br/artigos/node-js-definicao-caracteristicas-vantagens-usos)


---


### Javascript

- Criado para ser uma linguagem dinâmica voltada para o front
- Mas a galera do back começou a curtir e querer usar, só que eles não usam html
- Então surgiu a necessidade de uma forma de rodar o JavaScript que não use o html
- Daí surge o Node.js

---

## Node.js

-  funciona como um interpretador de JavaScript fora do ambiente do navegador web.
- ambiente de execução assíncrono, isto é, ele trabalha de modo a não bloquear no momento da execução da aplicação, delegando os processos demorados a um segundo plano
- torna possível o envio de instruções (os nossos códigos) sem precisar de um navegador ativo, basta ter o Node.JS instalado e utilizar o terminal para executar um programa construído em JavaScript.
-  o Node utiliza uma outra ferramenta chamada de Chrome's V8 JavaScript Engine. É esse motor V8 do Chrome que compila e executa o código JavaScript no lugar de apenas interpretá-lo.

### O Node é capaz de interpretar um código JavaScript, igual ao que o navegador faz.

---
### Empresas que usam Node.JS

- NetFlix: substituiu o renderizador baseado em Java pelo de JavaScript com Node.JS para diminuir o tempo de espera nas requisições dos usuários e conseguiu uma redução de cerca de 1 minuto;
- IBM: criou o The Canary in the Gold Mine (CITGM), um utilitário de testes amplamente utilizado pela companhia;
- LinkedIn: o aplicativo mobile da maior rede profissional é construído em Node e HTML5, você pode conferir a matéria na íntegra aqui;
- Uber: sistema de relacionamento entre usuários e condutores foi construído em Node.JS por conta de sua rápida resposta.
- Foi inclusive utilizado pela NASA

---

###  Como instalar o Node.js no Windows, Linux e macOS:


- https://www.alura.com.br/artigos/como-instalar-node-js-windows-linux-macos
- https://nodejs.org/en/download/


---



## Testando o Node

- Escolha algum arquivo .js que você já tenha feito
- Abra a pasta onde esse arquivo esta (use o explore normal)
	-  na barra de endereço clique na setinha e abra as opções, selecione node.js
- Para executar o código, envie o comando `./nome-do-arquivo.js`, ou `node nomedoarquivo.js`
- [Command-line API | Node.js v21.5.0 Documentation (nodejs.org)](https://nodejs.org/api/cli.html)
- https://nodejs.reativa.dev/0007-node-run-cli/index


---




## Extensões do vscode

[Extensões VS Code: descubra quais são as mais usadas | Alura](https://www.alura.com.br/artigos/extensoes-vs-code-descubra-as-mais-usadas?_gl=1*1oq6u4g*_ga*ODU1NjUxMDAxLjE3MDMxODU0ODA.*_ga_1EPWSW3PCS*MTcwNTA3NzQ5Ny4xMC4xLjE3MDUwNzc3MDMuMC4wLjA.*_fplc*MG9uMXlKcDB0dTZzTTZ5ZFQwMW92eTRDJTJGYTJ6ZDYyWXkzcmUxJTJCak55bGslMkJhc2JKZUhGJTJCWFJUbWYlMkJzQ1hPZ3JldU9RWlBJNGV3QklCNWlhaWNOZXRlMUdjZm0lMkJ5Uk9BZkVxVW84Y1p4S0paT0twN1ZaZHV6eFNUcGRQeGdRJTNEJTNE)

[Live Server - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)

- Extensões -> buscar ->  liveserver
- Name: Incredible Watcher: https://marketplace.visualstudio.com/items?itemName=AvisekhSabi.incrediblewatcher
- Name: vscode-pets: https://marketplace.visualstudio.com/items?itemName=tonybaloney.vscode-pets

---

## Lista de atalhos

https://www.brasilcode.com.br/atalhos-vs-code-25-atalhos-para-aumentar-sua-produtividade/


---




---

# Referências:

#javascript 
#nodejs 
