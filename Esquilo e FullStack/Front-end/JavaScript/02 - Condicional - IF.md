# Condicional: `if`

---

### Se essa (condição for verdadeira), então faça { isso }

### Se a condição for `true`, execute o código
---

```javascript
if ( condição) {
	código
}
```

```javascript
if ( ano == 2024) {
	alert("Feliz Ano Novo!")
}
```


---

## Else

## Se essa (condição for verdadeira), então faça { isso }
### Se for falsa, então faça { essa outra coisa }

## Se a condição for `true`, execute o código
### Se for `false`, execute esse outro código aqui

---
### Else

```javascript
if ( ano == 2024) {
	alert("Feliz Ano Novo!")
} else { 
	alert("Calma, já está acabando!)
}
```

---

## Else If

## Se essa (condição for `True`), então faça { isso }
### Se for `False`e se essa outra (condição for `True`), então faça { essa outra coisa }
### Se as duas forem `False`, então faça { essa outra coisa }

---
### Else if

```javascript
if ( mês == "Janeiro") {
	alert("Feliz Ano Novo!")
} else if (mês = "Dezembro" ) {
	alert("Calma, já está acabando!)
} else {
	alert("Vish, ainda falta muito para acabar!")	  
}

```
---
# Atividade

Usando o código da aula passada em que vocês perguntavam a idade do usuário, acrescente um If para avaliar se a pessoa é maior de 18 anos e faça uma janela alert para informar se ela é ou não maior de idade.

Depois, acrescente mais condições usando else e else if.

---
## Operadores lógicos

 ### OR: || 
 ### AND: &&
 ## NOT: !


- Mais em: https://javascript.info/logical-operators
---

## Atividade usando o operador lógico

---

# Operador condicional `?`

* Esse cara é estranho, e vocês vão demorar para usar, mas vale mencionar

---
## Ele é um `if` reduzido, um operador que permite que um valor seja atribuído há uma variável dependendo de uma condição

## Se a (condição) for `True` , então o `resultado` vai ser o `Valor1`
## Se a (condição) for `False` , então o `resultado` vai ser o `Valor2`


```javascript
resultado = condição ? Valor1 : Valor1;
```

---

##### Exemplo

```javascript
idade = prompt('Qual é a sua idade?');

mensagem = (idade < 3) ? 'Oi, bebê!' : 
// se a idade for menor do que 3, a mensagem = 'Oi bebê!'
(idade < 18) ? 'Olá!' :
// se a idade for menor do que 18 (e maior do que 3), a mensagem = 'Olá!'
(idade < 100) ? 'Saudações!' :
// se a idade for menor do que 100 (e maior do que 18), a mensagem = 'Saudações!'
'Que idade rara!';
// se todos os anteriores forem falsos, mensagem = 'Que idade rara!'

alert(mensagem );
```

---

### O mesmo código com if:

```javascript
if (idade < 3) {
  messagem = 'Oi, bebê!';
} else if (idade < 18) {
  messagem = 'Oi!';
} else if (idade < 100) {
  messagem = 'Saudações!';
} else {
  messagem = 'Que idade rara!';
}
```

---


## Tarefa

### Vamos fazer um código com condicional if:
### Crie um código que retorna o nome de uma jogadora da seleção feminina de futebol quando o usuário entra com o número da sua camiseta.

* Mais informações no próximo slide (copia isso no quadro)

---

## Algorítimo

- Janela prompt é aberta com a frase "Digite um número de 1 a 23";
- Usuário entra com um valor de 1 a 23;
- Se esse valor for igual a 1, publique a mensagem "Bárbara";
- Se esse valor for igual a 2, publique a mensagem "Antonia";
- Se esse valor for igual a 3, publique a mensagem "Kathellen Sousa"
- Se esse valor for igual a 4, publique a mensagem "Rafaelle";
- . . . . . 
- Se esse valor for igual a 23, publique a mensagem "Gabi Nunes";

* Lista de convocadas: https://trivela.com.br/futebol-feminino/copa-do-mundo-feminina/selecao-feminina-conheca-todas-jogadoras/
* Quando terminar, faça o mesmo código usando o operador `?`
- ![[tarefa-if-else.html]]


---



# Referências:

#javascript #if_javascript