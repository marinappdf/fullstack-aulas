
## Free code camp

Uma boa lista aqui: https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/


## **Projetos em JavaScript puro** 

1. [Como criar um trocador de cores](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-um-trocador-de-cores)
	 - arrays
	- document.getElementById()
	- document.querySelector()
	- addEventListener()
	- document.body.style.backgroundColor
	- Math.floor()
	- Math.random()
	- array.length
	
2. [Como criar um contador](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-um-contador)
	 - document.querySelectorAll()
	- forEach()
	- addEventListener()
	- currentTarget property
	- classList
	- textContent
1. [Como criar um carrossel de revisões](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-um-carrossel-de-revis-es)
	- objetos
	- DOMContentLoaded
	- addEventListener()
	- array.length
	- textContent
1. [Como criar uma navbar responsiva](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-uma-navbar-responsiva)
	- 
2. [Como criar uma sidebar](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-uma-sidebar)
3. [Como criar um modal](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-um-modal)
4. [Como criar uma página de perguntas frequentes](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-uma-p-gina-de-perguntas-frequentes)
5. [Como criar uma página de menu de restaurante](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-uma-p-gina-de-menu-de-restaurante)
6. [Como criar um segundo plano de vídeo](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-um-segundo-plano-de-v-deo)
7. [Como criar uma barra de navegação com rolagem](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-uma-barra-de-navega-o-com-rolagem)
8. [Como criar guias que exibem conteúdos diferentes](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-guias-que-exibem-conte-dos-diferentes)
9. [Como criar um relógio de contagem regressiva](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-um-rel-gio-de-contagem-regressiva)
10. [Como criar seu próprio Lorem ipsum](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-seu-pr-prio-lorem-ipsum)
11. [Como criar uma lista de compras](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-uma-lista-de-compras)
12. [Como criar um slider de imagens](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-um-slider-de-imagens)
13. [Como criar um jogo de pedra, papel e tesoura](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-um-jogo-de-pedra-papel-e-tesoura)
14. [Como criar o jogo do Genius](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-o-jogo-do-genius)
15. [Como criar um jogo de plataforma](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-um-jogo-de-plataforma)
16. [Como criar o Doodle Jump](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-o-doodle-jump-e-o-flappy-bird)
17. [Como criar o Flappy Bird](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-o-doodle-jump-e-o-flappy-bird)
18. [Como criar um jogo de memória](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-sete-jogos-cl-ssicos-com-ania-kubow)
19. [Como criar um jogo de Whack-a-mole](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-sete-jogos-cl-ssicos-com-ania-kubow)
20. [Como criar um jogo de Ligue 4](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-sete-jogos-cl-ssicos-com-ania-kubow)
21. [Como criar o jogo da cobrinha](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-sete-jogos-cl-ssicos-com-ania-kubow)
22. [Como criar o jogo do Space Invaders](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-sete-jogos-cl-ssicos-com-ania-kubow)
23. [Como criar o jogo do Frogger](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-sete-jogos-cl-ssicos-com-ania-kubow)
24. [Como criar o jogo de Tetris](https://www.freecodecamp.org/portuguese/news/40-projetos-em-javascript-para-iniciantes-ideias-simples-para-comecar-a-programar-em-js/#como-criar-sete-jogos-cl-ssicos-com-ania-kubow)


## Gustavo Guanabara

Tomando como base os trabalhos do Gustavo Guanabara, vamos fazer os exercícios dele de Javascript.


![[Front-end/JavaScript/Exercícios - Gustavo Guanabara/index.html]]

# Exercícios de JavaScript

Quer ter acesso aos códigos-fonte dos exercícios a seguir? Acesse a [área do repositório com todas as pastas e arquivos](https://github.com/gustavoguanabara/javascript/tree/master/exercicios) para cada exercício.

Temos ainda algumas apostilas para a complementação do aprendizado. Para acessá-las, vá até a [área do repositório com os arquivos PDF](https://github.com/gustavoguanabara/javascript/tree/master/aulas-pdf) com o material escrito.

## 📁 Nível 1: JavaScript básico

- [Ex001](app://obsidian.md/ex001/index.html): Olá, Mundo!
- [Ex002](app://obsidian.md/ex002/index.html): Interagindo com um botão
- [Ex003](app://obsidian.md/ex003/index.html): Usando o `prompt()` e o `alert()` juntos
- [Ex004](app://obsidian.md/ex004/index.html): Interagindo com partes do HTML (DOM)
- [Ex005](app://obsidian.md/ex005/index.html): Manipulando números
- [Ex006](app://obsidian.md/ex006/index.html): Somando dois números
- [Ex007](app://obsidian.md/ex007/index.html): Calculando a média de um aluno
- [Ex008](app://obsidian.md/ex008/index.html): Vários cálculos em JS
- [Ex009](app://obsidian.md/ex009/index.html): Contador de cliques
- [Ex010](app://obsidian.md/ex010/index.html): Usando um JavaScript externo

## 📁 Nível 2: Condições em JavaScript

- [Ex011](app://obsidian.md/ex011/index.html): Calculando média e dando 'Parabéns'
- [Ex012](app://obsidian.md/ex012/index.html): É par ou é ímpar?
- [Ex013](app://obsidian.md/ex013/index.html): Qual é o maior?
- [Ex014](app://obsidian.md/ex014/index.html): Interagindo com o sistema
- [Ex015](app://obsidian.md/ex015/index.html): Analisando data e hora
- [Ex016](app://obsidian.md/ex016/index.html): Calculando a idade
- [Ex017](app://obsidian.md/ex017/index.html): Sorteador de números
- [Ex018](app://obsidian.md/ex018/index.html): Jogo da adivinhação
- [Ex019](app://obsidian.md/ex019/index.html): O usuário escolhe o cálculo
- [Ex020](app://obsidian.md/ex020/index.html): Estações do ano

## 📁 Nível 3: Repetições em JavaScript

- [Ex021](app://obsidian.md/ex021/index.html): Contando de 1 até 10
- [Ex022](app://obsidian.md/ex022/index.html): Contando de 1 até 10 (marcando os pares)
- [Ex023](app://obsidian.md/ex023/index.html): Contando de 1 até 10 (mostrando só os pares)
- [Ex024](app://obsidian.md/ex024/index.html): Contagem regressiva de 10 até 1
- [Ex025](app://obsidian.md/ex025/index.html): Pegando dados de um formulário
- [Ex026](app://obsidian.md/ex026/index.html): Contagem mais "inteligente"
- [Ex027](app://obsidian.md/ex027/index.html): Tabuada
- [Ex028](app://obsidian.md/ex028/index.html): Fatorial de um número
