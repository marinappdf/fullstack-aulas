

## Para executar JavaScript:

---
### No Navegador:

1. **Console do Navegador:** Abra o console do seu navegador (normalmente pressionando F12 e selecionando a aba "Console"). Lá, você pode digitar comandos JavaScript diretamente ou incluir scripts na página.

2. **Incluir em uma Página HTML:** Você pode incluir scripts JavaScript dentro de tags `<script>` no arquivo HTML. Por exemplo:
---
#### Exemplo

```html
<!DOCTYPE html>
<html>
<head>
  <title>Exemplo</title>
</head>
<body>

<script>
  // Seu código JavaScript aqui
  console.log("Olá, mundo!");
</script>

</body>
</html>
```
---
### Com Node.js:

1. **Instale o Node.js:** Baixe e instale o Node.js no seu computador. Após a instalação, você poderá executar arquivos JavaScript no terminal.
2. **Crie um arquivo JavaScript:** Crie um arquivo com a extensão `.js` e escreva seu código JavaScript nele.
3. **Execute o arquivo com Node.js:** No terminal ou prompt de comando, navegue até o diretório onde seu arquivo JavaScript está localizado e use o comando `node nomeDoArquivo.js` para executá-lo.

- Ver [[06 - Node.js e extensões do VSCODE]]
---

Por exemplo, se você tiver um arquivo chamado `exemplo.js`, execute-o no terminal com:

```bash
node exemplo.js
```

O código dentro do arquivo será executado e qualquer saída usando `console.log()` será exibida no terminal.
Para acessar o terminal, entre na pasta em que o documento está, no endereço digite "cmd" e, daí sim, para rodar, digite `node nome-do-arquivo.js`

---

### Vamos tentar?
#### Baixar node.js
#### https://nodejs.org/en

---

## Dica de tutorial

- Guia do Programador
	- https://www.youtube.com/@GuiadoProgramador
	- https://www.youtube.com/watch?v=LLqq6FemMNQ&list=PLJ_KhUnlXUPtbtLwaxxUxHqvcNQndmI4B


---

# Referências:

#javascript #nodejs