
---
# Introdução ao JavaScript 
---

## Introdução

- Material sobre JavaScript
	- https://javascript.info/
-  Foi feito para tornar as páginas da web mais vivas


---

### O que o javascript pode fazer:


- Adicionar novo HTML à página, alterar o conteúdo existente, modificar estilos.
- Reajir às ações do usuário, executar cliques do mouse, movimentos do ponteiro, clicks de teclas.
- Enviar solicitações pela rede para servidores remotos, fazer download e upload de arquivos (os chamados [AJAX](https://en.wikipedia.org/wiki/Ajax_(programming)) e [COMET](https://en. wikipedia.org/wiki/Comet_(programming)) tecnologias).
- Obter e configurar cookies, faça perguntas ao visitante, mostre mensagens.
- Lembrar-se dos dados do lado do cliente (“armazenamento local”).

---

## Como funciona a estrutura desse negócio?

- Ele é uma coisa que a gente coloca dentro do html.

- Lembra que o CSS a gente também coloca no meio do HTML, sempre que a gente abre `<style>`?
	- Então, o JavaScript, assim como o CSS, é um apêndice, um anexo, um item extra do HTML.

---

### Por isso, sempre que forem chamar o JS, vocês irão abrir o tag (etiqueta) 
```html
<script> . . . </script>
```

ou

vão chamar um arquivo externo do tipo .js como um tributo da tag
```html
<script src="script.js">
. . . 
</script>
```
---

### Repare bem nesse trecho (atributo): `src="script.js"`

- Onde mais usamos `src`?
	- src = SouRce Code = Código Fonte

---

### Pode também ser um link: 

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.js">
. . . . 
</script>
```
---
### Como isso fica num código?

```html
<html> <body>
<p>Before the script...</p>
```
```javascript
<script>

    alert( 'Hello, world!' );

</script>

```
  ```html
<p>...After the script.</p>

</body></html>
```
![[olha-mundo.html]]




---
### Algo que vocês vão errar e nós vamos demorar para encontrar o erro:


```html
<script src="file.js">
  alert(1);
</script>
```

Por que isso não vai funcionar?

---

#### Ou chamamos um código externo `<script src="…">` ou incluimos algo no `<script>`.

- Para fazer os dois, eles precisam estar separados:

```html
<script src="file.js">
	. . . 
</script>
<script>
  alert(1);
</script>
```

---


#  1) Olá Mundo!


Tá, agora vamos tentar fazer um mini programa.

Vamos começar usando os tutoriais dessa página: https://javascript.info/hello-world

---

Vamos fazer uma janela que dá oi
```html
<html> <body>
<p>Before the script...</p>
```
```javascript
<script>

    alert( 'Hello, world!' );

</script>

```
  ```html
<p>...After the script.</p>

</body></html>
```
```

 - ![[olha-mundo.html]]
 - ![[olha-mundo-script-externo.html]]
 - ![[olha-mundo-script-externo-let.html]]


---

# Legal, próximo assunto

---

## Java Script trabalha com a manipulação de informações


---

## É preciso dar nome para cada pedaço de informação

## Qual o nome damos a isso?
## Como chamamos o tipo de nome dados às informações?

---


# V A R I Á V E I S

* MAIS INFORMAÇÕES EM: https://javascript.info/types


---

## Cada variável armazena uma informação, um dado diferente


## Variáveis são caixinhas organizadoras com etiquetas coloridas


---

## O comando para DECLARAR uma variável em JS é:

## LET

### Do inglês: deixar, permitir

#### Estamos dizendo "Deixa esse nome aí livre", ou "Permito que isso seja um nome"

- Imagens em: https://javascript.info/variables

---

## Treinando:

### Volte no código de "Olá mundo!" e crie uma variável chamada "mensagem" e altere o código de maneira a publicar a variável mensagem.

### Desafio: tente criar duas variáveis, mensagem 1 e 2, cada uma contendo uma parte da palavra. Publique tudo na mesma caixa de mensagem.

- ![[olha-mundo-script-externo-let.html]]

---

### Quais os nome possíveis?

* O nome deve conter apenas:
	* letras
	* dígitos 
	* símbolos: $ ou _
 
- O primeiro caracter não deve ser um dígito.

---

# CONSTANTES

Variáveis constantes não podem ter seu valor alterado.


```javascript
const myBirthday = '18.04.1982';

myBirthday = '01.01.2001';
// error, can't reassign the constant!
```


---

## Treinando: volte no código do Olá mundo!

1. Declarar duas variáveis: "saudacoes" e "informacao";
2. Atribuir o valor "Olá mundo!" para a variável "informacao";
3. Copie o valor de "informacao" para a variável "saudacoes";
4. Mostrar o valor de "saudacoes" usando o comando alert;

- Metáfora didática: máquina de suco

---
# Máquina de suco de laranja

![[maquinadesuco.webp]]

---

# Tipo de dados/informações

### Qual o tipo de variável de cada um?

# n = 5

# n = "5"

---


# n = 5 -> NÚMERO

# n = "5" ->  STRING 
### -> CORDA -> AMARRAÇÃO


---

### Palavras (string) devem estar cercadas por:

```javascript
let str = "Oie";
let str2 = 'Tchau';
let frase = `usada para add valor ${str2}`;
```

- Praticamente não há diferença no uso de citações duplas e únicas.
- Crase são citações de “funcionalidade estendida”. Eles nos permitem incorporar variáveis e expressões em uma cadeia usando 
	-  `${…}`

---

# Booleano

## True: SIM

## False: NÃO

---

### Existem outros tipos de variáveis, mas não vamos falar disso agora

- Primitivas:
    - `number` 
    - `bigint` 
    - `string` 
    - `boolean` 
    - `null` 
    - `undefined` 
    - `symbol`
- Não-primitiva:
    - `object` 
- Função para descobrir qual o tipo de variável
	- `typeof`


---

# Interação com o Usuário
## `alert`
## `prompt` 
## `confirm`

---

## `alert`

* Cria uma janela **modal** que impede o usuário de interagir com o resto da página até clicar em "ok".
* Janela de alerta!


---
## `prompt` 

* Janela modal que aceita entradas do usuário
* A função tem dois argumentos: o primeiro é o texto que aparece para o usuário, o segundo é opcional e determina o valor inicial/padrão do campo de entrada

```javascript
result = prompt(title, [default]);
```

* `result`é o nome da variável, caixa, onde a informação será guardada

---

## `confirm`


* Mostra uma janela modal com um `question` e dois botões: OK e Cancelar.
* OK -> True
* Cancelar -> False

```javascript
let pergunta = confirm("Isso é uma pergunta, tá ok?");


```

---

## Atividade

- Faça um pequeno programa que pergunta a idade do usuário, pede uma confirmação com a frase "Sua idade é . . .? " e depois abre uma mensagem de alerta com o retorno/resultado da confirmação.

---

## Atividade

```javascript
let idade = prompt('Quantos anos você tem?', 100);

let pergunta = confirm(`Você tem ${idade} anos de idade!`);

alert(pergunta); 

// alert("Você tem" + idade + "anos de idade!");
```

![[atividade-prompt-confirm.html]]

---

# Conversão do tipo de variável

### String -> Numeric

####  o usuário entra com um valor de texto e eu quero transformar em número
### Boolean -> String
 #### Quero transformar o resultado booleano em uma string

---

## Conversão para String: `String(info);`

## Conversão para Numeric: `Number(info);`

```javascript
alert( +x ); // 1, para x = 1

alert( +y ); // -2, para y = -2;

alert( +true ); // 1
alert( +"" );   // 0
```


## Conversão para Boolean: `Boolean()`
* 0, variáveis vazias ou `null`, `NaN`-> Falso
* Diferente de zero, variáveis com informação dentro -> True

---

# Operadores Matemáticos

##  `+`     `-`      `*`       `/`

##  `%`     `**`


---

## Somando na mesma variável

```javascript
let n = 1;
n = n + 5; // n = ?
n = n * 2; //n = ?
n += 5; // n = ? (o mesmo que n = n + 5)
n *= 2; //  n = ? (o mesmo que n = n * 2)
```

---

## Incremento  `++`

```javascript
let n = 2;
n++;   // equivalente a n = n + 1
```

## Decremento `--`


```javascript
let n = 2;
n--;   // equivalente a n = n + 1
```

* Alguém sabe para que isso serve?


---

## Concatenação de String

### Soma de textos, tu coloca a caixinha de um texto logo depois da outra

###  `f = "minha" + "variável"`

### `f = "isso" + " é " + "uma frase"

---

## Somando String e number: CUIDADO!

```javascript
alert( '1' + 2 );

alert( 2 + '1' );

alert(2 + 2 + '1' ); 

alert( "14" / "2" );

```

---

```javascript
alert( '1' + 2 ); // 12

alert( 2 + '1' ); // 21

alert(2 + 2 + '1' ); // "41" and not "221"

alert( "14" / "2" ); // 7

```

---

## Atividade -> Qual o resultado de cada uma delas?

```javascript
"" + 1 + 0
"" - 1 + 0
true + false
6 / "3"
"2" * "3"
4 + 5 + "px"
"$" + 4 + 5
"4" - 2
"4px" - 2
"  -9  " + 5
"  -9  " - 5

```

---
```javascript

"" + 1 + 0 = "10"
//  A adição com uma string "" converte 1 para uma string: `"" + 1 = "1"`, e então nós temos "1" + 0, a mesma regra é aplicada.

"" - 1 + 0 = -1 
//A subtração `-`(como a maioria das operações matemáticas) só funciona com números, converte uma cadeia vazia "" para 0

true + false = 1
//1 + 0 = 1

6 / "3" = 2
// Divisão converte a string para número

"2" * "3" = 6
// Multiplicação converte a string para número

```
---
```javascript
4 + 5 + "px" = "9px"
// A operação começa na esquerda, então inicia na soma numérica, depois encontra uma soma com string, então tudo vira string

"$" + 4 + 5 = "$45"
// A operação começã na esquerda, então 4 é forçado a virar string, e depois 5 é forçado a virar string

"4" - 2 = 2
// Subtração não converte número para string

"4px" - 2 = NaN
// Subtração não converte número para string, então ele tenta fazer com que 4px vire um número, só que ele não é um número, então dá ruim

"  -9  " + 5 = "  -9  5" 
// O operador soma permite que o número seja convertido para uma string

"  -9  " - 5 = -14
// O operador de subtração não permite a transformação em string, então tudo vira número
```

---

## Atividade

### Faça um código em que o usuário deve entrar com 2 número diferentes e o programa deve retornar o resultado da soma deles.

```javascript
let a = prompt("Primeiro número?");

// . . . . 

a = parseFloat(a)

// complete o código

```


---

## Comparações 

-  `a > b`
- `a < b`
- `a >= b`
- `a <= b`
- `a == b`
	- Qual a diferença para `a = b`?
-  `a != b`
	- `a ≠ b`

---
## O que elas retornam?
* Para a = 1 e b = 2 :

-  `a > b` 
- `a < b`
- `a >= b`
- `a <= b`
- `a == b`
-  `a != b`

---
## O que elas retornam?
* Para a = 1 e b = 2 :

-  `a > b`  -> False / Falso -> 0
- `a < b` -> True / Verdade -> 1
- `a >= b` -> False / Falso -> 0
- `a <= b` -> True / Verdade -> 1
- `a == b` -> False / Falso -> 0
-  `a != b` -> True / Verdade -> 1

---

## Comparação de Strings

```javascript
alert( 'Z' > 'A' ); 
alert( 'Mikael' > 'Michel' );
alert( 'Uhuu' > 'Uhu' ); 
alert( 'a' > 'A');
```


---

## Comparação de Strings

```javascript
alert( 'Z' > 'A' ); // True
alert( 'Michel' > 'Mikael' ); // False
alert( 'Uhuu' > 'Uhu' ); // True
alert( 'a' > 'A'); // True
```

---

# Fim



---
Export: é o comando que informa o que sera exportado, o resultado, da função em js.
	- https://developer.mozilla.org/en-US/docs/web/javascript/reference/statements/export
	- https://javascript.info/import-export




---
Debug -> F12


---



# Referências:

#javascript 