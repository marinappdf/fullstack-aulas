
# Objetos

---
### Objetos

- Objetos são um tipo especial de variável
- Um objeto é composto de um par:
	- Chave e Valor
	- key : value
- Key -> Uma string
- Value -> qualquer tipo de variável







----

### Objetos

- **permite armazenar e organizar informações de forma mais complexa**
	- Objeto: Carro
		- Chaves/Key: cor, modelo, ano, potencia do motor
		- Value:
			- cor: vermelho
			- modelo: mobi fiat
			- ano: 2021
			- potencia do motor: 1.0

---
### Objetos

- Objeto: Pasta de documentos (Uma pasta de pastas)
	- Key: nome de cada uma das subpastas
		- Value: Conteúdo das pastas
![[objeto-javascript.png]]imgem


---


## Código do objeto



```javascript
 let user = new Object(); // objetos constructos
let user = {};  // objetos literais
```

- Aqui criamos um Objeto chamado "user"
- O Objeto está vazio

---

### Código do Objeto

```javascript
let user = {     // criado o objeto chamado "user"
  nome: "João",  // na chave/key "nome" está o conteúdo "João" 
  idade: 30        // na chave/key está o conteúdo "30"
};
```


![[objeto1javascript.png]]imagem






---

### Código do Objeto

```javascript
alert( user.nome ); // João
alert( user.idade ); // 30
```


---

### Código do Objeto

```javascript
let user = {
  nome: "João",
  idade: 30,
  
};

alert(user.nome)
alert(user.idade)

user.futebol = confirm(`O ${user.nome} gosta de futebol?`);

alert( user.futebol);
alert( user[futebol] ); // Você pode igualmente usar os brackets
```
![[objetos-testes.html]]


---

## Atividade

- Se baseando na atividade da aula de loops, faça uma versão em que as entradas do usuário são guardadas em um objeto. No final, publique na tela as informações do usuário.
- As informações que você deve pedir são: nome, idade, salário
- Não se preocupe com as mensagens de erro, foque em entender como o objeto funcione, mas se você terminar, tente adicionar os loops como fizemos na última aula
- Estrutura geral do objeto:
```javascript
let user = { }; // criando um objeto vazio
user.nome = "João" // aqui o valor "João" é guardado com a chave nome dentro do objto user
``` 











---

## Funções

- Ok, entendemos brevemente os objetos, agora vamos falar sobre funções
- Eu sei que vocês já usaram algumas funções, mas vou aproveitar o tema de objetos para que a gente entenda melhor o que são as funções
- Funções são blocos de código criados para facilitar nossa vida.
- A gente pega um conjunto de código, dá um nome para ele e daí quando chamamos o seu nome ele vem e faz o seu trabalho
- Vocês fizeram isso com o botão, lembram?
- Fofoca: alert() e prompt() também são funções














---
### Estrutura de uma função

```javascript
function nome_da_funcao(parametro1, parametro2, ... parametroN) {
 // body
}
```
```javascript

function mostrarMensagem() {
  alert( 'E aí pessoal!' );
}


mostrarMensagem();
mostrarMensagem();
```

- Qual o nome dessa função?
- Quais os parâmetros da função?
- O que está escrito no corpo da mensagem?
- Como eu faço para chamar ela?







---
### Funções
```javascript
function showMessage(from, text) { // parameters: from, text
  alert(from + ': ' + text);
}

showMessage('Ann', 'Hello!'); // Ann: Hello! (*)
showMessage('Ann', "What's up?"); // Ann: What's up? (**)
```










---
### Funções

```javascript
function media(valor1. valor2, valor3) {

let med, med3

med = valor1 + valor2 + valor3

med3 = med/3

return med3
}

resuldado = media(2,4,6)

```

- Qual o nome dessa função?
- Quais os parâmetros da função?
- O que está escrito no corpo da mensagem?
- Como eu faço para chamar ela?
- Qual o valor que a função retorna?
- Qual o valor de resultado?
![[função-média.html]]


---

### Funções: variáveis globais/externas e locais/internas 

```javascript

function mostrarMensagem() {
	let mens = 'E aí pessoal!' 
	alert( mens );
}
mostrarMensagem();

mostrarMensagem(mens); // erro, mens só existe dentro do universo da função
```
---

### Atividade

- Crie uma função que calcule a média de 5 valores.
- Chame essa função e publique os resultados na tela (alert)

```javascript
function nome_da_funcao(parametro1, parametro2, ... parametroN) {
 // aqui vai o código
 return valor_de_saida
}


//assim se chamada a função
resultado = nome_da_funcao(p1, p2, . . .  pn)






```

---
### Funções geralmente tem um nome padrão

Por exemplo, as funções que começam com `"show"`Geralmente mostram alguma coisa.

Função que começam com ...

- `"get…"`– Retornam um valor,
- `"calc…"`– calculam alguma coisa,
- `"create…"`– criam alguma coisa,
- `"check…"`– verificam algo e devolvem um booleano, etc.
---
## Como faz para retornar dois valores?

- array
- dados
---
### Vamos fazer mais alguns exercícios

1. Faça uma função que retorne o número de caracteres de uma string
	1. A função deve ter um parâmetro (a string a ser analisada);
	2. Você pode usar o texto.length() para obter o tamanho
	3. A função deve retornar um número (o tamanho da string);
2. Faça uma função que crie uma frase com as palavras de entrada
	1. Por exemplo, os parâmetros de entrada podem ser um nome, idade  e altura. A frase criada pode ser do tipo `Oi, meu nome é ${nome}, eu tenho ${idade} e ${altura} metros de altura"
3. Faça uma função que coloca em um array apenas os números ímpares dentro do intervalo definido como parâmetro da função
	1. A função terá dois parámetros, o valor inicial e o valor final;
	2. O valor de saída é um array com números ímpares
	3. Os números ímpares podem ser analisados pelo resto da divisão:
		1. Quando o resto da divisão de um número por 2 é zero, o número é par, quando é diferente de zero, é ímpar
			- `if (numero%2 == 0) { array.push(numero)  //número par}
			- `else if ( numero % 2 != 3){//numero impar}`

---
### Chamando funções dentro de funções

```javascript
// pergunte("Você concorda?", mostrarOk, mostrarCancel);
function pergunte(pergunta, sim, nao) { 
  if (confirm(pergunta)) sim()//if(confirm("Você concorda?")) mostrarOK()
  else nao(); // else mostrarCancel()
}

function mostrarOk() {
  alert( "Você concordou." );
}

function mostrarCancel() {
  alert( "Você cancelou a execução" );
}

// Chamando a função pergunte com os parâmetros em ordem
pergunte("Você concorda?", mostrarOk, mostrarCancel);
```


---

### Função também podem ser chamadas como variaveis

- Note que a função foi criada no meio do código,  e que ela  pode ser manipulada como uma função
```javascript
let digaOi = function() { 
  alert( "Oi" );
};
let func = digaOi;

digaOi(); // Hello!
func();  // Hello!
```

![[funcoes-testes.html]]

---

### Funções também podem ser chamadas como variaveis
#### Enquanto variáveis normais guardam valores, funções guardam ações, então se quisermos usar uma ação no lugar de uma variável, podemos criar uma variável
```javascript
function ask(question, yes, no) {
  if (confirm(question)) yes()
  else no();
}

ask(
  "Do you agree?",
  function() { alert("You agreed."); },
  function() { alert("You canceled the execution."); }
);
```

---

### Atividade

- Faça um código que pergunte ao usuário (cookies) se ele aceita alguns cookies;
- Crie a opção dele aceitar a oferta (ok) ou negar ela (cancel);
- Faça isso criando pelo menos três funções diferentes: 1 para a pergunta, outra para o ok e uma terceira para o cancel.


- Desafio EXTRA: se baseando no código da aula sobre loopings, crie uma função que execute a ação de avaliar  se a resposta do usuário está correta ou não. Dica: comece alterando apenas uma pergunta.
- A ideia é que o código tenha duas funções, uma que faz uma pergunta para o usuário e outra que avalia se a entrada do usuário é válida.
- Nos moldes de?
```javascript
function perguntarInformação (pergunta, condicao){
 // aqui vai pedir a informação para o usuario
while (condicao(resultado))
  { //aqui vai mostrar o erro caso a entrada do usuario estiver errada  }

function condicao_nome(entrada)
{ // aqui vai verificar se a entrada do usuario está correta }
```

![[Funções-erro-mensagem.html]]
---
### Código:

```javascript

function perguntarInformação (pergunta, condicao){
  let resultado = prompt(pergunta);
  while (condicao(resultado))
  {
    alert("Informação incorreta, tente de novo.")
    resultado = prompt(pergunta)      }
    alert("Informação correta")}

function condicao_nome(entrada)
{ let saida
  if (entrada.length<3){ saida = true}
  else { saida = false}
  return saida
}

function condicao_idade(entrada){
  let saida
  entrada=parseFloat(entrada)
    if (entrada>100){ saida = true    }
    else { saida = false}
    return saida}

perguntarInformação("Qual o seu nome", condicao_nome)
perguntarInformação("Qual a sua idade", condicao_idade)

```


---
# Objetos e Funções

---
### Objetos e funções

- É muito comum no js o uso de funções e objetos. Vamos analisar um exemplo em que a função fazerUsuario cria um objeto com duas chaves/keys, nome e idade, e que aceita como entrada o valor dessas duas propriedades. 
```javascript
function fazerUsuario(nome, idade) {
  return {
    nome: nome,
    idade: idade,
    // ...outras propriedades
  };
}

let usuario = fazerUsuario("Carol", 30);
alert(usuario.nome); // Carol
alert(usuario.idade)
```

---

# Objetos, cópias

- https://javascript.info/object-copy
- Importante entendermos que o Objeto não guardo o valor em si, mas um endereço para ele
	- É com se o o objeto fosse um documento que guarda a informação do endereço do valor
	- Em comparação, uma variável (primitiva) comum guarda o conteúdo em si.
	- Enquanto as variáveis primitivas são um copo que guarda o suco, o objeto guarda o endereço de onde o suco está.
---
### Qual a diferença?

- Quando fazemos isso com variáveis primitivas, criamos uma cópia idêntica, mas distinta da original.
	- Ou seja, temos dois conteúdos guardados em duas variáveis distintas
- Quando fazemos isso com objetos, é criada uma cópia do endereço, da refêrencia, mas não é criada uma cópia do endereço.
	- Ou seja, temos apenas um conteúdo, que pode ser acessado por dois objetos/referências diferentes

![[objeto-copiasy.png]]

---
## Tá, e daí?

- A diferença é que, ao fazer uma cópia de um objeto eu cópio o endereço, então se eu mudar um, mudo o outro tabém
	- Eles passam a estar interligados!

```javascript
let user = { nome: 'Carol' };

let admin = user;

admin.nome = 'Gabriel'; // mudando pela referência do admin

alert(user.name); // ele muda também pela referência do user
// Vai aparecer 'Gabriel', não 'Carol'
```

---

### Eles são equivalentes?

- Uma consequência disso é que dois objetos só são idênticos quando são uma cópia um do outro.
	- Portanto:
```Javascript
let a = {};
let b = a; // copy the reference

alert( a == b ); // true, ambos referenciam o mesmo objeto


let a = {};
let b = {};
alert( a == b ); //false, eles tem o mesmo conteúdo (nada), mas não são equivalentes
```

- Como acontecem com as primitivas?
- (ps: ao comprar objetos com primitivas, os bjetos se comportam como primitivas)
---

### E se eu quiser clonar

- Object.assign()
- structuredClone()

----
## Aquecimento

- Crie um objeto de uma cafeteira que tenha como chaves/keys/propriedades o modelo, a cor e a marca.
- Publique cada uma das propriedades com console log
	- F12 -> console

---
### Aquecimento 
```javascript
let cafeteira = {
	modelo: "DolceGusto",
	cor: "vermelha",
	marca: "Nescafé"
}

console.log(cafeteira.modelo, cafeteira.cor, cafeteira.marca)
```

---

## Objetos: métodos

- https://javascript.info/object-methods
- Objetos: geralmente usado para representar entidades do mundo real (usuários, ordens de compra)
- Logo, eles também podem agir: usuários podem fazer login, log out, adicionar itens ao carrinhos, etc.
- Funções: são blocos de ação
- Métodos são as ações dos objetos

---
### Revisando

- Variáveis são caixas de informação
- Objetos são entidades complexas com propriedades, características
- Funções são ações
- Métodos são as ações dos objetos
	- Métodos são as funções associadas aos objetos.

---

### Exemplo

```javascript
// Criando o objeto
let user = {
  nome: "Carol",
  idade: 30
};

// Declarando uma função normal
function digaOi() {
  alert("Oi!");
}

// Declarando ela como uma propriedade do user
user.digaOi = digaOi;
// Chamando o método do objeto
user.digaOi(); // Oi!
```
 
---
### Ambos chegam no mesmo resultado:

```javascript
//Jeito 1
user = {
  digaOi: function() {
    alert("Oi"); 
    console.log("Oi") },
    modelo: "Dulce Gusto"
  cor: "Vermelha"};

user.digaOi() // isso chama o método digaOi


// Jeito 2
user = {
  digaOi() { 
    alert("Oi");  }}
```
---
## O uso de "this" no método


- https://javascript.info/object-methods#this-in-methods
- É possível fazermos funções genéricas para objetos.
- Para isso usamos o this. , ele entende que this. é a entrada de um objeto genérico

```javascript
let user = {
  name: "John",
  age: 30,

  sayHi() {
    // "this" is the "current object"
    alert(this.name);
  }

};

user.sayHi(); // John
```

---

### this. é tipo

```javascript
let user = {
  name: "John",
  age: 30,

  sayHi() {
    alert(user.name); // "user" instead of "this"
  }

};
```
---

### this.

```javascript
let user = { name: "John" };
let admin = { name: "Admin" };

function sayHi() {
  alert( this.name );
}

// use the same function in two objects
user.f = sayHi;
admin.f = sayHi;

// these calls have different this
// "this" inside the function is the object "before the dot"
user.f(); // John  (this == user)
admin.f(); // Admin  (this == admin)
```

---
### Tarefa com this.
1) Crie um método que, usando o this., publique o conteúdo das propriedades/chaves/keys de um objeto (faça com a cafeteira)
	- Chame o objeto.método()
	- Dica:
```javascript
let user = {
  name: "John",
  age: 30,
  sayHi() {
    alert(this.name); }};
```
2) Faça uma calculadora usando objetos e métodos, especialmente o .this
		- Você pode acessae https://javascript.info/object-methods para estudar e se basear nos exemplos disponíveis
		- Depois de finalizado, faça uma função para a divisão.
		- 
![[calculadora-this0objetos.html]]

---


# Fim -> passar para class

---

### Próximos slides devem ser revisados

---
## Constructor -> operador "new"

- Esse cara aqui vai nos ajudar a criar vários usuários similares
	- Vários usuários
	- Vários itens de um menu

### Função construtora
- Funções construtoras são tecnicamente funções regulares. Existem duas convenções:
	- Eles são nomeados com letra maiúscula primeiro.
	- Eles devem ser executados apenas com "new"operador.

---


### Como assim?

```javascript
// função Construtura com um parâmetro
// Note que o this. torna ela genérica para qualquer objeto

function User(name) {
  this.name = name;
  this.isAdmin = false;
}
// ele é criado aqui de maneira normal, com o novo nome e o padrão false para o isAdmin
let user1 = new User("Jack");

alert(user1.name); // Jack
alert(user1.isAdmin); // false

let user2 = new User("Ana")
alert(user2.name); // Ana
alert(user2.isAdmin); // false


```

---

### Métodos também podem ser inseridos nos contrutores

```javascript
function User(name) {
  this.name = name;

  this.sayHi = function() {
    alert( "My name is: " + this.name );
  };
}

let john = new User("John");

john.sayHi(); // My name is: John


//john = {  name: "John", sayHi: function() { ... }}

```






---

### Atividade (suspensa)

1. Crie uma função que crie um objeto com as seguintes keys/chaves: nome, sobrenome, idade e telefone.
2. Usando o prompt(), peça para o usuário entrar com cada uma dessas informações e grave elas em um objeto
3. Mostre as informações contidas no objeto criado.

![[função-objetos.html]]

---

# Referências:

#javascript 
#object_javascript #function_javascript