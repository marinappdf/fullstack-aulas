JavaScript possui diversas funções integradas que são amplamente utilizadas para diferentes propósitos. Aqui estão algumas das funções integradas mais comuns e úteis:

1. **`alert()`**: Mostra uma caixa de diálogo com uma mensagem para o usuário.

2. **`console.log()`**: Exibe mensagens no console do navegador ou terminal, útil para depuração e verificação de valores.

3. **`setTimeout()` e `setInterval()`**: `setTimeout()` executa uma função após um determinado tempo, enquanto `setInterval()` executa uma função repetidamente em intervalos regulares.

4. **`document.querySelector()` e `document.querySelectorAll()`**: Selecionam elementos no DOM usando seletores CSS.

5. **`addEventListener()`**: Adiciona um ouvinte de eventos a um elemento HTML para responder a ações do usuário.

6. **`Array()` e `Array.of()`**: Criam novos arrays.

7. **`Array.prototype.map()`**, **`Array.prototype.filter()`**, **`Array.prototype.reduce()`**: Métodos de array para mapear, filtrar e reduzir arrays.

8. **`String.prototype.split()`** e **`String.prototype.join()`**: `split()` divide uma string em um array com base em um separador, enquanto `join()` une os elementos de um array em uma string.

9. **`Math.random()`** e **`Math.floor()`**: `Math.random()` gera um número decimal aleatório entre 0 e 1, e `Math.floor()` arredonda para o número inteiro mais baixo.

10. **`fetch()`**: API moderna para fazer requisições HTTP/AJAX.

11. **`JSON.parse()`** e **`JSON.stringify()`**: `JSON.parse()` converte uma string JSON para um objeto JavaScript, e `JSON.stringify()` converte um objeto JavaScript para uma string JSON.

Essas são apenas algumas das muitas funções integradas ao JavaScript. Cada uma delas desempenha um papel específico e pode ser combinada com outras funcionalidades para realizar tarefas complexas.

12. **`Date()`**: Cria um objeto que representa uma data e hora.
    
13. **`Object.keys()`** e **`Object.values()`**: `Object.keys()` retorna um array contendo as chaves de um objeto, enquanto `Object.values()` retorna um array com os valores das propriedades de um objeto.
    
14. **`Promise()`**: Cria um objeto Promise, que representa um valor que pode não estar disponível ainda, mas que será resolvido eventualmente ou rejeitado.
    
15. **`String.prototype.substring()`** e **`String.prototype.slice()`**: Ambos retornam uma parte de uma string, mas com comportamentos ligeiramente diferentes.
    
16. **`Math.max()`** e **`Math.min()`**: `Math.max()` retorna o maior número em um conjunto de números, enquanto `Math.min()` retorna o menor.
    
17. **`parseFloat()`** e **`parseInt()`**: `parseFloat()` analisa um argumento string e retorna um número de ponto flutuante, enquanto `parseInt()` analisa um argumento string e retorna um inteiro.
    
18. **`encodeURIComponent()`** e **`decodeURIComponent()`**: `encodeURIComponent()` codifica um componente Uniform Resource Identifier (URI), enquanto `decodeURIComponent()` decodifica um componente codificado.
    
19. **`Number()`**: Converte um valor para um número, se possível.
    
20. **`RegExp()`**: Cria um objeto de expressão regular para correspondência de padrões em strings.
----
# Referências:

#javascript #function_javascript 