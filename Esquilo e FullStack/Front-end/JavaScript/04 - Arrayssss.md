
# Array
---

# O que é um array?


- https://javascript.info/array
- Uma coleção ordenada
- Permite manipular os itens, reordenar, etc.
- É uma caixa organizadora e etiquetada

imagem:
![[estrutura-array.png]]

---
#### O que mais pode ser?


![[array3d.png]]
imagem

---
#### Outro olhar

![[arraysnd.jpg]]
imagem

---

#### Podemos fazer contas também


![[array-operaçõesmatematicas.png]]
imagens


---


#### Ou seja, Array é um vetor, uma matriz, com N dimensões possíveis

### E qual a sua cara no Javascript?

```javascript
let nome_do_array = new Array("Item 1", "Item 2", "Item 3");
let nome_do_array = ["Item 1", "Item 2", "Item 3"];
```
- Qual a cara de cada um deles?

---

### Índices: cada item tem um endereço, o índice

![[estrutura-array.png]]


---

### O que vai aparecer aqui?
```javascript
let nome_do_array = ["Item 1", "Item 2", "Item 3"]
alert(nome_do_array[0])
alert(nome_do_array[1])
alert(nome_do_array[2])











```
---
### Atividade: fazer um Array de produtos com 5 itens.

- Usem o alert para publicar cada um deles

![[estrutura-array.png]]











---

#### Podemos substituir um elemento de maneira similar a que fazemos com as variáveis comuns

```javascript
nome_do_array[1] = 'novo item'
```
#### Faça isso na sua lista de produtos: substitua um item por um novo item

---

#### É possível também adicionar um novo item: basta escolher um índice maior do que o tamanho do array

```javascript
nome_do_array[3] = 'item extra'
```
#### Faça isso na sua lista, experimente adicionar um número

---
#### Array são muito usados como filas

- `.pop`: Extrai o último elemento do array
- `.push`: Anexe o elemento ao final do array:

```javascript
let lista = ["item 1", "item 2", "item 3"];

lista.pop(); // remove o último item (item3)

alert( lista); // vamos ver apenas os itens 1 e 2

lista.push("novo item 3")

alert( lista); // vamos ver 3 itens, os dois originais e o novo

```
#### Faça o mesmo com a sua lista

---

#### Array são muito usados como filas

- `.shift`: Extrai o primeiro elemento do array e o retorna
- `.unshift`: Adicione o elemento ao início do array:

```javascript
let lista = ["item 1", "item 2", "item 3"];

lista.shift(); // remove o primeiro item (item1)

alert(lista); // vamos ver apenas os itens 2 e 3

lista.unshift("novo item 1")

alert( lista); // vamos ver 3 itens, os dois novos e um original

```
#### Faça o mesmo com a sua lista


---

### Atividade: use um looping for para mostrar (alert) todos os itens da lista, um por um


- nome_do_array.length
- for (começo ; condição; passo)

### Faça um looping que adicione apenas número ímpares à um array.
 - de 1 a 50
 - Dica: use o for, o % e o push









---

# For . . . OF

Tem um looping especial para isso:

```js
let fruits = ["Apple", "Orange", "Plum"];

// iterates over array elements
for (let fruit of fruits) {
  alert( fruit );
}

```







---

### Matrizes

```js
let matrix = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9]
];

alert( matrix[1][1] ); //
```

- Batalha Naval






---
### Atividade

- Gente, vamos tentar fazer um código interessante usando array.
- Mas antes, vamos remlemebrar uma forma de printar na tela os resultados do JS: 
No html

```html
<html>
<p>O item 1 do array: <span id="item0"></span></p>
```
No Javascript


```js
<script>
document.getElementById('item0').innerText = frutas
</scrip>
```








---
## Atividade

### 1) Escreva na tela cada um dos itens da do array

### 2) Usando um looping for, escreva um código que escreva na tela cada um dos itens do array



- Vou mostrar aqui:
![[array.html]]

---

# Referências:

#javascript #arrays_javascript