
# Skill<>

###  Marina de Freitas
#### Entrada : 11 de Dezembro de 2023

Janeiro 2024

---

## Objetivo

### Formação FullStack
### Formação Backend

---

## Outubro, Novembro e Dezembro


| Contúdo Básico | Horas lecionadas |
| ---- | ---- |
| Marketing | 8h |
| Educação fiscal e direitos trabalhistas | 8h |
| Trabalho e renda | 4h |
| Informática básica | 2h |
| Inclusão digital | 4h |
| Raciocínio Lógico e matemático | 4h |
| Segurança do trabalho | 4h |

---

## Outubro, Novembro e Dezembro

| Conteúdos Específicos | Horas Lecionadas |
| ---- | ---- |
| História da Programação- | 8h |
| Informática Básica- | 11h |
| Introdução ao desenvolvimento web (HTML, CSS, JavaScript e DOM) | 36h |
| Algoritimo e lógica de prgramção | 30h |
| Controle de versões | 4h |
| Design Thinking | 4h |

---

## Dezembro e Janeiro


| Contúdo Básico | Horas Lecionadas |
| ---- | ---- |
| Consciência Política | 4h |
| Inglês Instrumental | 4h |

---

## Dezembro e janeiro
| Conteúdo Específico | Horas Lecionadas |
| ---- | ---- |
| Desenvolvimento Web: HTML e CSS | 40h |
| Desenvolvimento Web: JavaScript e DOM | 40h |

- Incluído no desenvolvimento web está o aprofundamento na lógica e estrutura de programação:
	- Condicionais
	- Laços
	- Vetores/Arrays
	- Programação Orientada ao Objeto

---

## Janeiro

| Conteúdos Específicos | Horas Lecionadas |
| --- | --- |
| Programação em Java | 35h |

- Em Java, estão sendo trabalhadas os seguintes tópicos:
	- Desenvolvimento ocm o software Intelej
	- Aprofundamento da programação orientada ao objeto;
	- Conceito de pacotes 
	- Modificadores (public/private), métodos Getter e Setters
	- Herança e polimorfismo
	- Interfaces
	- Arrays e Listas de dados
	

---
## Fevereiro 
#### Semana 1 (05, 06, 07, 08, 09/02)
| Conteúdo Específico | Conteúdo Básico |
| ---- | ---- |
| Desenvolvimento em Java | Mundo do Trabalho |
| Ordenamento de listas com pacotes e interfaces (Collections.sort, comparable, List, linkedlist) |  |

#### Semana 2 (14, 15, 16/02)
| Conteúdo Específico | Conteúdo Básico |
| ---- | ---- |
| Desenvolvimento em Java | Protagonismo e cidadania |
| Consolidando conhecimentos: mini-projeto com as bibliotecas java.lang, java.util, java.io, Java Collections |  |

---

## Fevereiro
#### Semana 3 ( 19, 20, 21, 22, 23/02)
| Conteúdo Específico | Conteúdo Básico |
| ---- | ---- |
| SQL com MySQL e Server da Oracle | Juventude, esporte e lazer |
| Introdução ao SQL |  |
| Consultas com SQL |  |

#### Semana 4 (26, 27, 28, 29/02, 01/04) 

| Conteúdo Específico | Conteúdo Básico |
| ---- | ---- |
| Comandos com DML | Sustentabilidade |
| Procedures com SQL |  |

---

## Março

#### Semana 1 (4,5,6,7,8/03)

| Conteúdo Específico | Conteúdo Básico |
| ---- | ---- |
| DBA (Database Administrator): Segurança  e Otimização do banco | Comunicação |
| Consolidando Conhecimentos: Revisão e Projeto Final de MySQL |  |
#### Semana 2 (11, 12, 13, 14, 15/03)

| Conteúdo Específico | Conteúdo Básico |
| ---- | ---- |
| Entendendo e conhecendo o SpringFramework | Trabalho em equipe |
| Javaweb: Java Servlet,  Maven, Java Database Connectivity (JDBC) |  |

---
## Março
#### Semana 3 (18, 19, 20, 21, 22/03)

| Conteúdo Específico | Conteúdo Básico |
| ---- | ---- |
| Entendendo e conhecendo o SpringFramework | Leitura e compreensão de textos |
| Java Persistence API: Persistência, Consultas avançadas, Performance, Modelos complexos |  |
#### Semana 4 (25, 26, 27, 28, 29/03)

| Conteúdo Específico | Conteúdo Básico |
| ---- | ---- |
| Entendendo e conhecendo o SpringFramework | Noções de estatística e gráficos |
| Spring Data Spring MVC, Thymeleaf, Spring Security, API Rest AJAX |  |
| Consolidando Conhecimentos em Spring Framework |  |

 
---

## Abril

| Conteúdo Específico | Conteúdo Básico |
| ---- | ---- |
| TCA: planejamento e estudos | Diversidade cultural brasileira |
| Definição do escopo do projeto, definição das equipes |  |
| Planejamento do desenvolvimento do projeto, atribuição de tarefas e funções |  |
| Protótipo: construindo o protóptipo do algoritmo |  |
| Identificando lacunas de conhecimento e especificando os conteúdos focados para o projeto |  |

## Maio

| Conteúdo Específico | Conteúdo Básico |
| ---- | ---- |
| TCA: desenvolvimento do projeto | Projetos de vida |

---


# Obrigada!

---