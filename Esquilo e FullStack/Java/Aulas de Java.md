# Referências
#java #backend #orientado-a-objeto #alura
# Aulas alura
historia: https://www.alura.com.br/artigos/java?_gl=1*5zdpgg*_ga*MTg4ODYyMTU3LjE3MDMxNjMxNjU.*_ga_1EPWSW3PCS*MTcwNTMxNjcyOC4xNC4xLjE3MDUzMTc2NzcuMC4wLjA.*_fplc*RHBod2dSa2h5V2NTcEFYVmp6RWxBRGUzbW15WjB4SHBiJTJGQmxXWGR2UG53WTY3UTNaNFpDVFdWUkV4VmYyM3NoYSUyQlBlNzZPUyUyRk83ckJhS0NDMzk0cENtMTRXazYlMkZ0bVN6RXA2UFVhSGV1R3NuVHJyVzhGS3pUanVsNnYlMkIzdyUzRCUzRA..

https://cursos.alura.com.br/extra/hipsterstech/ecossistema-java-revisitado-hipsters-ponto-tech-313-a1597


https://cursos.alura.com.br/formacao-boas-praticas-java

https://cursos.alura.com.br/course/java-pacotes-e-java-lang


https://cursos.alura.com.br/formacao-threads-java

https://cursos.alura.com.br/course/java-consumindo-api-gravando-arquivos-lidando-erros

https://cursos.alura.com.br/formacao-certificacao-java

---

# Aulas realizadas

https://cursos.alura.com.br/course/java-criando-primeira-aplicacao/task/124285
[Java: trabalhando com listas e coleções de dados: Aula 1 - Atividade 1 Apresentação | Alura - Cursos online de tecnologia](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126223)


---

# Aula 2 - 16/01

## Terminando a Atividade da aula anterior

criar uma class em que o usuário entra com 3 notas e retorna uma média delas.

Exercício em: [Java: criando a sua primeira aplicação: Aula 3 - Atividade 6 Avaliando o filme | Alura - Cursos online de tecnologia](https://cursos.alura.com.br/course/java-criando-primeira-aplicacao/task/124292)


```java
import java.util.Scanner;

public class Loop {
    public static void main(String[] args) {
        Scanner leitura = new Scanner(System.in);
        double mediaAvaliacao = 0;
        double nota = 0;

        for (int i = 0; i < 3; i++) {
            System.out.println("Diga sua avaliação para o filme  ");
            nota = leitura.nextDouble();
            mediaAvaliacao += nota;
        }

        System.out.println("Média de avaliações " + mediaAvaliacao/3);

    }
}
```

## Assistindo o módulo "Um modelo para representar filme" dentro de Java: aplicando a Orientação a Objetos.

Aulas assistidas:
- [Java: aplicando a Orientação a Objetos: Aula 1 - Atividade 2 Um modelo para representar filmes | Alura - Cursos online de tecnologia](https://cursos.alura.com.br/course/java-aplicando-orientacao-objetos/task/125814)
	- Classes
- [Java: aplicando a Orientação a Objetos: Aula 1 - Atividade 3 Atribuindo valores ao nosso filme | Alura - Cursos online de tecnologia](https://cursos.alura.com.br/course/java-aplicando-orientacao-objetos/task/125815)
	- Objetos
- [Java: aplicando a Orientação a Objetos: Aula 1 - Atividade 4 Manipulando objetos | Alura - Cursos online de tecnologia](https://cursos.alura.com.br/course/java-aplicando-orientacao-objetos/task/126173)
	- Métodos

Filme meuFilme = new Filme()

Nome da classe nome do objeto = new nome-da-classe();
 
meuFilme.nome = "O podero chefão"
nome-do-objeto.atributo

métodos 

void exibeFichaTecnica () {
System.out.println( nome)
}

dai na class main: meuFilme.exibeFichaTecnica()

Não colocou o this . . . .  vejamos

## Próximas etapas

Em seguida, vamos para a etapa 2, consolidando o conhecimento sobre programação orientada a objetos: https://cursos.alura.com.br/course/java-aplicando-orientacao-objetos
-> concluído

## Atividade prática: criar um novo atributo e método

Criar o atributo : "classificacaiIndicativa" e o método "verifiqueClassificacaoIndicativa(idade)".
Os estudantes demonstram dificuldades em criar o código passada em aula pela alura. Vejo que é importante dar um tempo para que eles recriem cada código. Talvez o ideal é fazer isso depois de cada vídeo, para que eles acompanhem.

---



# Aula 3 17/01

## Revisão aula anterior

- Verificar se todos terminaram a atividade da aula anterior
- Na class Filme, fazer um atributo "classificacãoIndicativa" e um método "avalieClassificacaoIndicativa"
- Um forma de fazer isso é criar o método
```java
	//boolean
	void avalieClassificacaoIndicativa(int idade){
	boolean permissao = idade >= classificacaoIndicativa
	//System.out.println(permissao)
	if (permissao) {System.out.println("Permissão concedida.")}
	else {System.out.println("Permissão negada."}
	//return permissao
	}	
boolean avaliarCI (int idade){
return  idade >= classificacaoIndicativa
}
permissao = avaliaCI(12)
 mensagemDeErro(avaliaCI(12))
```
## Recordar é viver

- O que aprendemos ontem?
- Boolean: tipo let, retorna se é verdadeiro ou falso
- aprendomos o que são int e double
- Tem que deixar dentro do colchete para funcionar
- aprendemos a mexer no intelj
- aprendemos o que é void, o vazio da existência
- aprendeu a usar class e objetos, aprendeu como programa

## Conteúdo da aula: Módulo 2, 02, 03, 04,. . . . 

[Java: aplicando a Orientação a Objetos: Aula 2 - Atividade 2 Configurando o que pode ser visto e modificado | Alura - Cursos online de tecnologia](https://cursos.alura.com.br/course/java-aplicando-orientacao-objetos/task/125817)



Encapsulamento.
Tornar privado.

Definir o método ou atributo como privite ou public.

```java
private double atributoDaClass;
```
Isso eu defino na classe. E no objeto eu não consigo chamar.


Então, se a pessoa não pode mexer, eu preciso criar um método, na classe, que permite que seja acessado. Por isso temos o getters e saetters.

```java
int getDeAvaliacoes{
return atributoDaClass
}
```
O nome disso é modificador, o privete, é o modificador.
Java tem outros modificadores

![[04 Para saber mais- modificadores de acesso - getters e setters.pdf]]
### Java DOC

[Overview (Java SE 17 & JDK 17) (oracle.com)](https://docs.oracle.com/en/java/javase/17/docs/api/index.html)

A documentação do Java, conhecida como **JavaDoc**, é uma referência completa para todas as classes, interfaces e métodos disponíveis na plataforma Java. Ela está disponível online e pode ser acessada neste [site da Oracle](https://docs.oracle.com/en/java/javase/17/docs/api/index.html).

O JavaDoc é organizado em pacotes, que contêm diversas classes e interfaces relacionadas a alguma funcionalidade específica da linguagem. Cada classe e interface é documentada individualmente, com informações detalhadas sobre sua funcionalidade, uso e exemplos de código.

Essa documentação deve ser uma referência essencial para qualquer pessoa que queira programar na linguagem Java, pois fornece informações detalhadas sobre como usar as classes e interfaces fornecidas pela linguagem. Tendo acesso à documentação, podemos também descobrir novas classes e métodos que podem ser úteis em nossos projetos, para resolver problemas comuns e recorrentes do dia a dia.


### Pacotes

Pacotes servem para organizar classes.
O nome é de grande para o menor.
Exemplo
br.com.alura.screenmatch.modelo

criado o modelo, preciso fazer import para chamar ele

package é o que define onde a classe estaá
```java
// na class
package br.com.alura.srcreenmatch.modelo.Filme
//no principal
import br.com.alura.srcreenmatch.modelo
```
Eu preciso ir no atributos e colcoar public. Em pacotes diferentes não é automático que é público. Então tem que escrever explicitamente qual é o modificador, se é public e privado
### Para saber mais: padrão de nomes de pacotes

[PRÓXIMA ATIVIDADE](https://cursos.alura.com.br/course/java-aplicando-orientacao-objetos/task/126181/next)

- https://cursos.alura.com.br/suggestions/new/java-aplicando-orientacao-objetos/126181/question

Em Java, pacotes são usados para agrupar classes que estão relacionadas a alguma funcionalidade. Eles ajudam a organizar o código, facilitando o seu gerenciamento e evitando que centenas de classes distintas fiquem todas misturadas num único diretório.

A organização de classes dentro de um pacote também pode ser feita usando subpacotes ou colocando as classes diretamente dentro do pacote, dependendo da complexidade do projeto. Além disso, é importante seguir a convenção de nomeação de pacotes para garantir que os pacotes sejam facilmente identificados.

Em relação à nomenclatura de pacotes, outro aspecto importante é o uso do nome de **domínio reverso** da empresa ou organização como parte do nome do pacote. Por exemplo, se o nome de domínio da empresa fosse "minhaempresa.com.br" e o projeto Java fosse chamado de "meuprojeto", o nome do pacote de domínio reverso seria **br.com.minhaempresa.meuprojeto**, sendo que dentro desse pacote principal podemos ter diversos subpacotes, para melhor organização do código do projeto.

Isso ajuda a garantir que o nome do pacote seja exclusivo e evita conflitos de nome com outros pacotes em outros projetos. Além disso, o uso do nome de domínio reverso como parte do nome do pacote também ajuda a identificar facilmente o proprietário do projeto Java.


### Olha a manha

Setters e Getters

Alt+insert

Botão direito -> generate -> setter -> qual atributo tu quer deixar ela criar?

Criado o setter, a forma commque eu vou atribuir valor muda.
Aon invés de dizer
```java
meufilme.nome = "o poderoso chefão"
```

Vai dizer
```java
meuFilme.setNome("o poderoso chefão")
```
considerando que foi criado na class Filme o método

```java
public void setNome(nome)
{
this.nome = nome
}
```
### Atividade de agora

1. Criar um pacote para organizar as classes.
2. Nome do pacote: br.com.alura.screenmatch.model
3. Vamos colocar o a class Filme dentro do Pacote
4. O intelej vai nos perguntar se queremos fazer o refactor: sim, queremos!
5. Isso automaticamente atualizar o restante do programa: vai adicionar os comandos `package` e `import` nos seus devidos lugares.
6. Agora precisamos definir os métodos e atributos como publics e privates
7. Vamos criar getters and setters
8.  Para criar getters e setters podemos usar o atalho  "Allt+insert", ou, manualmente irmos pelo menu: Botão direito -> generate -> setter -> qual atributo tu quer deixar ela criar?
9. Ir no Principal e chamar os metodos e atributos das formas adequadas.

### Qual a diferença entre getters e setters?

Get é pegar, então getters é pegadores
Set é definir, então setter é definidores.



# Aula 4 - 18/01 - Desafio em Java

## Retomando a atividade de ontem


1. Criar um pacote para organizar as classes;
2. Nome do pacote: br.com.alura.screenmatch.model;
3. Vamos colocar o a class Filme dentro do Pacote;
4. O intelej vai nos perguntar se queremos fazer o refactor: sim, queremos!
5. Isso automaticamente atualizar o restante do programa: vai adicionar os comandos `package` e `import` nos seus devidos lugares;
6. Agora precisamos definir os métodos e atributos como publics e privates
7. Vamos criar getters and setters;
8. getetters: 
	1. um para exibir as informações do filme (nome, ano, duração)
9. setters:
	1. defenir o nome
	2. definir o ano de lançamento
	3. definir a duração em minutos
10.  Para criar getters e setters podemos usar o atalho  "Allt+insert", ou, manualmente irmos pelo menu: Botão direito -> generate -> setter -> qual atributo tu quer deixar ela criar?
11. Ir no Principal e chamar os metodos e atributos das formas adequadas.


-> Conseguimos finalizar?
-> Link do projeto da aula anterior: https://github.com/alura-cursos/2887-java-screenmatch-oo/tree/aula02

##   Desafio

Vamos aplicar na prática os conceitos de modificadores de acesso public e private, import, this, getters e setters. Para reforçar essas habilidades, propomos uma lista de atividades práticas (**não obrigatórias**), enriquecendo ainda mais sua experiência de aprendizado.

Vamos colocar esses conceitos em ação?
Projeto 1
1. Crie uma classe `ContaBancaria` com os seguintes **atributos**: numeroConta (privado), saldo (privado) e titular (publico). 
2. Implemente métodos getters e setters para cada um dos atributos privados.

Projeto 2
1. Crie uma classe `idadePessoa` com os atributos privados nome e idade.
2. Crie métodos getters e setters para acessar e modificar esses atributos. 
3. Adicione um método verificarIdade que imprime se a pessoa é maior de idade ou não.

Projeto 3
1. Desenvolva uma classe `Produto` com os atributos privados nome e preco.
2. Utilize métodos getters e setters para acessar e modificar esses atributos. Adicione um método aplicarDesconto que recebe um valor percentual e reduz o preço do produto.

Projeto 4
1. Desenvolva uma classe `Aluno` com os atributos privados nome e notas.
2. Utilize métodos getters e setters para acessar e modificar esses atributos. 
3. Adicione um método calcularMedia que retorna a média das notas do aluno.

Projeto 5
1. Desenvolva uma classe `Livro` com os atributos privados titulo e autor.
2. Utilize métodos getters e setters para acessar e modificar esses atributos. 
3. dicione um método exibirDetalhes que imprime o título e o autor do livro.

# Aula 5 - 19.01 - 


## Recordar é viver

- O que aprendemos ontem?
	- aprendeu a usar o github pelo intelej
	- nem sempre precisa abrir uma segunda instância void.
	- classes privados a pessoa não tem acesso a informação
	- aprendeu como usa o scanner e o boolean
	- Revisamos da aula anterior
	- Não aprendi nada né, não consegui fazer
	- o public static void serve para chamar os métodos dentro do void


##   Desafio

Vamos aplicar na prática os conceitos de modificadores de acesso public e private, import, this, getters e setters. Para reforçar essas habilidades, propomos uma lista de atividades práticas (**não obrigatórias**), enriquecendo ainda mais sua experiência de aprendizado.

Vamos colocar esses conceitos em ação?
Projeto 1
1. Crie uma classe `ContaBancaria` com os seguintes **atributos**: numeroConta (privado), saldo (privado) e titular (publico). 
2. Implemente métodos getters e setters para cada um dos atributos privados.

Projeto 2
1. Crie uma classe `idadePessoa` com os atributos privados nome e idade.
2. Crie métodos getters e setters para acessar e modificar esses atributos. 
3. Adicione um método verificarIdade que imprime se a pessoa é maior de idade ou não.

Projeto 3
1. Desenvolva uma classe `Produto` com os atributos privados nome e preco.
2. Utilize métodos getters e setters para acessar e modificar esses atributos. Adicione um método aplicarDesconto que recebe um valor percentual e reduz o preço do produto.

Projeto 4
1. Desenvolva uma classe `Aluno` com os atributos privados nome e notas.
2. Utilize métodos getters e setters para acessar e modificar esses atributos. 
3. Adicione um método calcularMedia que retorna a média das notas do aluno.

Projeto 5
1. Desenvolva uma classe `Livro` com os atributos privados titulo e autor.
2. Utilize métodos getters e setters para acessar e modificar esses atributos. 
3. dicione um método exibirDetalhes que imprime o título e o autor do livro.




## Aula expositiva do Alura: Java, aplicando a orientação a objetos 

## 03: Reaproveitando características -> 02 Aproveitando o modelo para séries

- Java: aplicando a orientação a objetos
	- 02Aproveitando o modelo para séries
	- [Java: aplicando a Orientação a Objetos: Aula 3 - Atividade 2 Aproveitando o modelo para séries | Alura - Cursos online de tecnologia](https://cursos.alura.com.br/course/java-aplicando-orientacao-objetos/task/125821)
	- herança
		- Criar um tipo maior, grande, um supertipo, e daí depois especializa em cada coisa
		- incluir novos atributos da série
		- nome da coisa grande: título -> série e filmes
		- criar um super título maior que faça sentido -> animais -> cachorros e gatos
		- criar no pacote modelos uma nova class chamada de título: o título vai ter tudo que estava no filme, vamos tirar tudo que estava no filme e colocar no título
		- No Java, utilizamos a **palavra reservada "extends"** para aplicar a herança. Assim, definimos que `Filme` estende `Titulo`:
```java
public class Filme extends Titulo 
```
	- 

- vamos criar a classe "Serie" no pacote de modelos, empregando a palavra reservada "extends"

```java
public class Serie extends Titulo
```
no principal:
```java
Serie serie = new Serie();
```

```java
Serie lost = new Serie();
lost.setNome("Lost");
lost.setDataDeLancamento(2000);
lost.exibeFichaTecnica();
```


nem precisa colcoar igual

```java
public class Serie extends Titulo {
    private int temporadas;
    private boolean ativa;
    private int episodiosPorTemporada;
    private int minutosPorEpisodio;
}
```

- Para poder definir e obter os valores desses atributos, vamos inserir os métodos _getters_ e _setters_ também. Basta pressionar "Alt + Insert", escolher a opção "Getter e Setter" e selecionar todos os atributos que queremos:

![[02 Aproveitando o modelo para séries.pdf]]


###  03 Mesmo nome, comportamentos diferentes -  https://cursos.alura.com.br/course/java-aplicando-orientacao-objetos/task/125822


Na verdade, queremos **sobrescrever** o método `getDuracaoEmMinutos()`.


```java
// Serie.java
// ...

@Override
public int getDuracaoEmMinutos() {
    return temporadas * episodiosPorTemporada * minutosPorEpisodio;
}

// isso retorna a mesma coisa que está na mae... ...
```

Termos sinônimos que aparecem com frequência são:

- Superclasse e subclasse
- Classe Mãe e Classe Filha

Assim como:

- Atributos e Campos
- Métodos e Funções





![[03 Mesmo nome, comportamentos diferentes.pdf]]

![[04 Para saber mais-entendendo melhor a herança.pdf]]

![[05 Para saber mais- anotações no Java.pdf]]


###   06 Evitando código duplicado

https://cursos.alura.com.br/course/java-aplicando-orientacao-objetos/task/125823

- criar um novo pacote
- dentro do pacote, criar uma nova class
- escrever os códigos de cálculo
- métodos para calcular o tempo de maratonar

```java
// CalculadoraDeTempo.java

package br.com.alura.screenmatch.calculos;

import br.com.alura.screenmatch.modelos.Filme;
// note que tem que importar a class dos modelos


public class CalculadoraDeTempo {
    private int tempoTotal;

    public int getTempoTotal() {
        return tempoTotal;
    }

    public void inclui(Filme f) {
        tempoTotal += f.getDuracaoEmMinutos();
    }
}
```

no principal

```java
// Principal.java

import br.com.alura.screenmatch.calculos.CalculadoraDeTempo;
import br.com.alura.screenmatch.modelos.Filme;
import br.com.alura.screenmatch.modelos.Serie;

public class Principal {
    public static void main(String[] args) {

    // código omitido

    CalculadoraDeTempo calculadora = new CalculadoraDeTempo();

    }
}
```

```java
// Principal.java

// ...

    CalculadoraDeTempo calculadora = new CalculadoraDeTempo();
    calculadora.inclui(meuFilme);
    System.out.println(calculadora.getTempoTotal());

// ...
```

```java
// Principal.java

// ...

Filme outroFilme = new Filme();
outroFilme.setNome("Avatar");
outroFilme.setAnoDeLancamento(2023);
outroFilme.setDuracaoEmMinutos(200);

CalculadoraDeTempo calculadora = new CalculadoraDeTempo();
calculadora.inclui(meuFilme);
System.out.println(calculadora.getTempoTotal());

// ...
```

para evitar duplicar código

```java
// ...
// recebe todos os herdeiros de título
public void inclui(Titulo titulo) {
    this.tempoTotal += titulo.getDuracaoEmMinutos();
}

// ...
```



![[06 Evitando código duplicado.pdf]]

![[07 Para saber mais- o modificador protected.pdf]]

### 08 Herdando classes




![[08 Herdando classes.pdf]]


---


# Aula 6 - 22.01

## Recordar é viver




- O que fizemos na sexta?
	- Polimorfismo: muitos + origem -> Classes e Herenças
	- É possível criar uma classe "mãe" e dela herdar filhas
	- Tipo um modelo geral que várias outras classes podem usar
	- Classe mãe: animal -> fazer sons, se alimentar, nome
		- Classe filha: cachorro -> latir, 
		- Classe filha: gato -> miar, faz planos de dominação do mundo

- Para criar uma class mãe, se cria normal, não tem diferença, a mesma coisa que criar uma classe normal
```java 
public class Animal {}
```
- a diferença é se for herdar
```java
public class Gato extends Animal {
// coisas extras que só gatos fazem
}

public class Cachorro extends Animal {
// coisas extras que só cachorros fazem
}
```

O que são pacotes?
- Pacotes são formas de agruparmos códigos e funcionalidades em pacotes, em bibliotecas
- Assim, quando quiser usar uma aplicação específica, ao invés de recriar o código, ou de copiar o código, eu vou só importar o pacote


Qual a diferença entre atributos e métodos de uma classe?
- Atributos são campos -> Propriedades, características, substantivos
- Métodos são ações -> são funções, são coisas que a class consegue fazer
- Cachorro.nome: atributo
- Cachorro.getNome(): método -> Cachorro.pegarNome
- Cachorro.setNome(Parâmetro-da-função): define o nome do cachorro

Funções
- Parâmetros da função são as entradas
- Estão sempre entre os parênteses das funções/métodos


## 04 - Aplicando ocmportamentos em comum 02 Incluindo comportamentos adicionais

[Java: aplicando a Orientação a Objetos: Aula 4 - Atividade 2 Incluindo comportamentos adicionais | Alura - Cursos online de tecnologia](https://cursos.alura.com.br/course/java-aplicando-orientacao-objetos/task/125825)

- Não tem como ter mais de uma herança, só podemos usar o extend de uma única class

### Interfaces

```java
public interface Classificavel {
	int getClassificacao
}
```
Note que `interface` são diferentes de `class`. 
Elas são como um método comum.
Dentro das interfaces temos métodos.
Geralmente o nome da interface é um **adjetivo.

```java
public class Filme extends Titulo implements Classificavel{
// . . . 

	public int getClassificacao(){
		return pegaMedia()/N;
	}
}

```
![[02 - Incluindo comportamentos adicionais.pdf]]

![[02 - Incluindo comportamentos adicionais.pdf]]

![[04 Para saber mais - entendendo melhor interfaces.pdf]]



### 08 Desafio: hora da prática


Vamos fazer apenas o 1:

Vamos explorar os fundamentos das interfaces no Java. Em uma interface, todos os métodos são automaticamente públicos, eliminando a necessidade da palavra reservada public em suas declarações. Além disso, assim como na herança, é possível aplicar o polimorfismo por meio de interfaces, proporcionando flexibilidade e coesão no código.

Para fixar melhor seu aprendizado, propomos uma lista de atividades práticas (**não obrigatórias**). Prontos para colocar esse conhecimento em prática?

1. Crie uma classe `ConversorMoeda` que implementa uma interface `ConversaoFinanceira` com o método converterDolarParaReal() para converter um valor em dólar para reais. A classe deve receber o valor em dólar como parâmetro.
	- Abrir um novo projeto
		- Abrir um Principal.java onde vou chamar e rodar os comandos principais
	- Cria uma interface chamada `ConversaoFinanceira`
		```java
		public interface ConversaoFinanceira{
			double converterDolarParaReal }
		```
2. Criar uma classe chamada `ConversorMoeda`
```java
	 public class ConversorMoeda implement ConversaoFinanceira{
		double valorDolar;
		public void setValorDolar(valorDolar) {
			this.valorDolar = valorDOlar;
		}
		public double getValorDolar(){
			System.out.println(valorDolar)
			return this.valorDolar
		}
		// Implementação da interface:
		public double converterDolarParaReal(valorDolar){
		double valorDolar, valorReal;
		valorReal = valorDolar*4.93;
		System.out.println(valorDolar + " dolares são " + valorReal + " reais.")
		return valorReal;
	 }
```
- No arquivo principal:
```java
	ConversorMoeda objeto-conversor-moeda = new ConversorMoeda();

	setValorDolar(5.89); 

	converterDolarparaReal(objeto-conversor-moeda.valorDolar);



```


Outros desafios:
1. Crie uma classe `CalculadoraSalaRetangular` que implementa uma interface `CalculoGeometrico` com os métodos calcularArea() e calcularPerimetro() para calcular a área e o perímetro de uma sala retangular. A classe deve receber altura e largura como parâmetros.
2. Crie uma classe `TabuadaMultiplicacao` que implementa uma interface `Tabuada` com o método mostrarTabuada() para exibir a tabuada de um número. A classe deve receber o número como parâmetro.
3. Crie uma interface `ConversorTemperatura` com os métodos celsiusParaFahrenheit() e fahrenheitParaCelsius(). Implemente uma classe `ConversorTemperaturaPadrao` que implementa essa interface com as fórmulas de conversão e exibe os resultados.
4. Crie uma interface `Calculavel` com um método double calcularPrecoFinal(). Implemente essa interface nas classes `Livro` e `ProdutoFisico`, cada uma retornando o preço final considerando descontos ou taxas adicionais.
5. Crie uma interface `Vendavel` com métodos para calcular o preço total de um produto com base na quantidade comprada e aplicar descontos. Implemente essa interface nas classes `Produto` e `Servico`, cada uma fornecendo a sua própria lógica de cálculo de preço.


## Começo de um novo desafio: [Java: aplicando a Orientação a Objetos: Aula 5 - Atividade 1 Construindo uma nova aplicação | Alura - Cursos online de tecnologia](https://cursos.alura.com.br/course/java-aplicando-orientacao-objetos/task/126199)



**Jacqueline:** Nesse curso exploramos vários aspectos da orientação a objetos, como herança, polimorfismo e encapsulamento. Destacamos os principais conceitos que você precisa se familiarizar para trabalhar com Java ou outras linguagens orientadas a objetos.

O nosso desafio é que você consiga praticar parte do que vimos, pensando, por exemplo, em uma **aplicação de músicas**, na qual seja possível escutar algum áudio. A ideia é que você tenha uma** superclasse**, que pode se chamar **"Áudio"**, e crie **suas heranças**, como **"Podcasts"**, **"Músicas"**, trabalhando essa questão do encapsulamento.

Vamos fazer uma aplicação, trabalhar herança, criar uma classe que controle suas músicas e podcasts preferidos e assim por diante. A ideia é modelar de maneira que exista uma classe "Áudio"** com título, duração, total de reproduções, curtidas e classificação, além de métodos encapsulados para curtir, reproduzir e assim por diante, impedindo que a classe Principal tenha acesso a esses atributos.**

Desafio você a começar a modelar essas classes e pensar nos atributos. Daqui a pouco vou trazer uma sugestão de como isso pode ser feito. Até lá!


Quais classes devem existir?
- Áudio
	- Músicas
	- Podcast
Quais atributos e métodos cada classe deve ter?
- Áudio
	- Atributos
		- duração em minutos
		- título
		- autoria
		- visualização
		- número de curtidas
	- Métodos
		- getters de todos eles
		- setters de todos
 
- Músicas
	- Atributos
		- generoMusical
	- Métodos
		- fazerPlaylist
- Podcast
	- Atributos
		- classificaçãoIndicativa / Faixa etária
	- Métodos
		- verificar idade do usuário
	

---

# Aula 23/01 -  Desafio de Java + ArrayList

## Recordar é viver

- Onde paramos ontem?
- Conversão financeira e não sei o que moeda
- Interface: muito parecido com herança -> um tio em comum
- Classes mãe: animal e plantas
	- Fotografável é uma interface que se aplica aos dois, mas não é herdade de nenhuma class
-  Projeto das músicas, dos streamings
- Vamos rever o código que escrevemos e continuar.

## Conteúdo expositivo

### Java - listas e coleções de dados - aula 03
https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126224



A declaração de variáveis com `var` possui algumas limitações:

1. O tipo da variável deve ser inferido automaticamente pelo compilador. Isso significa que não é possível utilizar `var` em variáveis cujo tipo não possa ser inferido automaticamente.
    
2. Não é possível usar `var` em variáveis sem valor inicial. É necessário atribuir um valor à variável na mesma linha em que ela é declarada.
    

A declaração de variáveis com `var` é uma funcionalidade relativamente nova no Java, mas que pode trazer diversos benefícios para o código, como a redução de digitação e melhor legibilidade do código. No entanto, é importante lembrar que existem limitações e que nem sempre é possível utilizar esse recurso.


```java
//Filme filmeDoPaulo = new Filme();  
var filmeDoPaula = new Filme();
```



- Construtor é um set obrigatório.





---
----

# Aula 24/01 - Desafio de Java


## Recordar é viver

- ArrayList
	- como criar um vetor em java
- toString
- Entendemos melhor interface
- Construtor: é um set obrigatório
	- Recurso para poder encapsular

## Desafio: hora da prática 

Para reforçar a compreensão sobre construtores, sugerimos atividades práticas (**não obrigatórias**) que aprofundarão seus conhecimentos fundamentais.

Pronto para mergulhar nesse aprendizado prático?

1. Crie uma classe `Produto` com atributos como nome, preco, e quantidade. Em seguida, crie uma lista de objetos Produto utilizando a classe ArrayList. Adicione alguns produtos, imprima o tamanho da lista e recupere um produto pelo índice.
- Abrindo um projeto novo
- Limpar o projeto padrão
- Criar o arquivo class: botão direito + new class + nome da classe "Produto"
- Criar a class: 
```java
public class Produto { 
// isso é a nossa class
}
```
- Criar atributos
```java
public class Produto { 

// tipo de acesso + tipo da variável + nome do atributo
private String nome;
private double preco;
private int quantidade;
}
```

- Ir para o arquivo Main/Principal para criar nosso objeto e nossa lista

```java
public class Main {  
    public static void main(String[] args) {

// tipo de variável + nome da minha variável = o valor da minha variável
//int  produtoI =  9;
// o tipo de class + nome do meu objeto = criando o objeto do tipo class específica
Produto produto1 = new Produto();
Produto produto2 = new Produto();
Produto produto3 = new Produto();
Produto produto4 = new Produto();


// Agora que eu criei meus objetos, euq uero criar uma lista desses obejtos

// vou colocar o import java.util.ArrayList no topo do arquivo

// int num = 9;
// Produto produto4 = new Produto();
// estruturando a mémoria que vais usada em listaDeProdutos
ArrayList<Produto> listaDeProdutos = new ArrayList<Produto>();


listaDeProdutos.add(produto1);
listaDeProdutos.add(produto2);
listaDeProdutos.add(produto3);
listaDeProdutos.add(produto4);


//System.out.println(listaDeProdutos.get(2).getNome)
// Isso vai dar errado
// Para conseguir fazer funcionar, precisamos voltar na class Produto e criar os getter and setters
// Alt + Insert (ou botão direito + Generate) -> escolher a opção "Getter and Setters" + selecionar os atributos desejados 
// Aí sim, o sout vai funcionar

for (int i=0; i<4 ; i=i+1)  
{  
  System.out.println(i);  
  System.out.println(listaDeFilmes.get(i).getNome()); 
}



}}
 
```

# Aula 25/01 - Desafio Java

## Recordar é viver

## Apresentar o Desafio: Continuação do desafio da aula passada 


2. Implemente o método toString() na classe `Produto` para retornar uma representação em texto do objeto. Em seguida, imprima a lista de produtos utilizando o método System.out.println().

3. Modifique a classe `Produto` para incluir um construtor que aceite parâmetros para inicializar os atributos. Em seguida, crie objetos Produto utilizando esse novo construtor.

4. Crie uma classe `ProdutoPerecivel` que herde de `Produto`. Adicione um atributo dataValidade e um construtor que utilize o construtor da classe mãe (super) para inicializar os atributos herdados. Crie um objeto ProdutoPerecivel e imprima seus valores.



# Aulas dos dias 26/01 e 29/01 - Desafio Array Lista e Conteúdo de Listas de Objetos Distintos
## Tarefa de HomeOffice 
###  Realizar todas as Atividades do tópico 03 -  lista de objetos distintos

Link da Aula: https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126678
Também disponível no nosso plano de ensino "Aula Presencial"

No item [08 - Desafio: hora da prática](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/147673), faça pelo menos os desafios 01 e 02.

Eu estarei online e disponível para tirar dúvidas, fiquem a vontade para entrar em contato comigo.

- [01Projeto da aula anterior](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126678)
- [02Tipos diferentes de objetos na lista14min](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126228)
- [03Para saber mais: outras formas de percorrer a lista](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126679)
- [04Identificando o objeto08min](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126229)
- [05Para saber mais: variáveis e referências](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126680)
- [06Referências de objetos](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126681)
- [07Faça como eu fiz: utilizando foreach e instanceof](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126682)
- [08Desafio: hora da prática](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/147673)
- [09O que aprendemos?](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126683)

---

#
# Próximas aulas . . . 

### Múdulo 2 - em andamento

### Modulo 3
Na terceira etapa, vamos mexer com listas de dados e arrays: https://cursos.alura.com.br/course/java-listas-colecoes-dados

## Atividade prática: 

### Atividade Prática A
Vimos como usar comparações para condicionais, integrar leitura do teclado com a classe Scanner, explorar alternativas para condicionais e empregar estruturas de repetição como for e while. Esses conhecimentos constituem a base para o desenvolvimento eficaz de programas em Java.

Para aprimorar essas habilidades, propomos atividades práticas (**não obrigatórias**). Vamos lá?

1. Crie um programa que solicite ao usuário digitar um número. Se o número for positivo, exiba "Número positivo", caso contrário, exiba "Número negativo".
2. Peça ao usuário para inserir dois números inteiros. Compare os números e imprima uma mensagem indicando se são iguais, diferentes, o primeiro é maior ou o segundo é maior.
3. Crie um menu que oferece duas opções ao usuário: "1. Calcular área do quadrado" e "2. Calcular área do círculo". Solicite a escolha do usuário e realize o cálculo da área com base na opção selecionada.
4. Crie um programa que solicite ao usuário um número e exiba a tabuada desse número de 1 a 10.
5. Crie um programa que solicite ao usuário a entrada de um número inteiro. Verifique se o número é par ou ímpar e exiba uma mensagem correspondente.
6. Crie um programa que solicite ao usuário um número e calcule o fatorial desse número.
---



## 10 Desafio: hora da prática

Vamos explorar na prática conceitos cruciais da herança em programação orientada a objetos: extends, a palavra-chave indicando que uma classe herda de outra; a habilidade de sobrescrever métodos herdados, utilizando a anotação @Override.

Além disso, vamos aplicar o conceito de polimorfismo para evitar duplicação de métodos, proporcionando uma estrutura de código mais eficiente e coesa.

Para consolidar essas ideias, propomos atividades práticas (**não obrigatórias**) que enriquecerão ainda mais sua compreensão desses princípios fundamentais. Pronto para colocar esses conceitos em prática?

1. Crie uma classe `Carro` com métodos para representar um modelo específico ao longo de três anos. Implemente métodos para definir o nome do modelo, os preços médios para cada ano, e calcular e exibir o menor e o maior preço. Adicione uma subclasse `ModeloCarro` para criar instâncias específicas, utilizando-a na classe principal para definir preços e mostrar informações.
2. Crie uma classe `Animal` com um método emitirSom(). Em seguida, crie duas subclasses: `Cachorro` e `Gato`, que herdam da classe `Animal`. Adicione o método emitirSom() nas subclasses, utilizando a anotação @Override para indicar que estão sobrescrevendo o método. Além disso, adicione métodos específicos para cada subclasse, como abanarRabo() para o Cachorro e arranharMoveis() para o Gato.
3. Crie uma classe `ContaBancaria` com métodos para realizar operações bancárias como depositar(), sacar() e consultarSaldo(). Em seguida, crie uma subclasse `ContaCorrente` que herda da classe `ContaBancaria`. Adicione um método específico para a subclasse, como cobrarTarifaMensal(), que desconta uma tarifa mensal da conta corrente.
4. Crie uma classe `NumerosPrimos` com métodos como verificarPrimalidade() e listarPrimos(). Em seguida, crie duas subclasses, `VerificadorPrimo` e `GeradorPrimo`, que herdam da classe `NumerosPrimos`. Adicione um método específico para cada uma das subclasses, como verificarSeEhPrimo() para o `VerificadorPrimo` e gerarProximoPrimo() para o `GeradorPrimo`.


![[10 Desafio hora da prática.pdf]]


## Aula de Git do Luan e do Agner


Aula do Luan
