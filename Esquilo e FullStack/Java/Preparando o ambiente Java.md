
https://www.oracle.com/br/java/technologies/downloads/


Temos os botões "JRE for Consumers" e "Open JDK Early Access Builds".

Então, são dois perfis, o "JRE for Consumers" que vai apenas executar a aplicação Java, o consumidor, que vai precisar somente daquele ambiente de execução, que virá com uma máquina virtual — em breve falaremos sobre ela — e algumas bibliotecas básicas para que seja possível executar a aplicação.


recisaremos de ferramentas, compilador, depurador, etc. Precisaremos do **Java Development Kit** (kit de desenvolvimento) ou JDK.