public class ConversorMoeda implements ConversaoFInanceira {
    public double valorDolar;

    public void setValorDolar(double valorDolar) {
        this.valorDolar = valorDolar;
    }

    public double getValorDolar() {
        return valorDolar;
    }
// Implementação da interface:

       @Override
    public double converterDolarParaReal(double valorDolar) {
        double valorReal;
        valorReal = valorDolar*4.93;
        System.out.println(valorDolar + " dolares são " + valorReal + " reais.");
        return valorReal;
    }
}