import clasMae.Audio;
import clasMae.Musica;
import clasMae.Podcast;

public class Main {
    public static void main(String[] args) {

        Musica musica1 = new Musica();
        Podcast podcast1 = new Podcast();

        // Completar:

        musica1.setTitulo("30 Semanas");
        musica1.setAutoria("Rico Dalassan");
        musica1.setDuracaoEmMinutos(3.33); //3min21s
        musica1.setGeneroMusical("Queer Rap");



        podcast1.setTitulo("Divã da Diva");
        podcast1.setAutoria("Diva Depressão");
        podcast1.setDuracaoEmMinutos(45);

        podcast1.setClassificacaoIndicativa(16);


        // Só devem ser chamados quando os botões são clicados
        //    musica1.contadorDeVisualizacao();
        //    musica1.contadorDeNumeroDeCurtidas();
        //podcast1.contadorDeVisualizacao();
        //podcast1.contadorDeNumeroDeCurtidas();
        int i = 0;
        for (i=0;i<10;i=i+1){
            musica1.contadorDeNumeroDeCurtidas();
            musica1.contadorDeVisualizacao();
            podcast1.contadorDeNumeroDeCurtidas();
            podcast1.contadorDeVisualizacao();
            //System.out.println(i);
        }
        //System.out.println(i);

        System.out.println("Podcast");
        System.out.println(podcast1.getTitulo() + " " + podcast1.getAutoria() + " " + podcast1.getNumeroDeCurtidas() + " " + podcast1.getVisualizacao() + " " +  podcast1.getDuracaoEmMinutos());

        System.out.println("Música");
        System.out.println(musica1.getTitulo() + " " + musica1.getAutoria() + " " + musica1.getDuracaoEmMinutos() + " " + musica1.getNumeroDeCurtidas() + " " + musica1.getVisualizacao() + " " + musica1.getGeneroMusical());

        // e o método específico do áudio?
        musica1.fazerPlaylist();

        // E os métodos, funcionam?

        podcast1.verificarIdade(12);


    }
}