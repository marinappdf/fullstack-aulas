package clasMae;

public class Musica extends Audio {

    private String generoMusical;

    public void fazerPlaylist() {
        System.out.println("Aqui se faz uma playlist");
    }

    public String getGeneroMusical() {
        return generoMusical;
    }

    public void setGeneroMusical(String generoMusical) {
        this.generoMusical = generoMusical;
    }
}
