package clasMae;

public class Audio {
    // Atributos, propriedades, características
    private double duracaoEmMinutos;
    private String titulo;
    private String autoria;
	private int visualizacao;
    private int numeroDeCurtidas;

    // Métodos, funções, ações da classe
    public double getDuracaoEmMinutos() {
        return duracaoEmMinutos;
    }

    public void setDuracaoEmMinutos(double duracaoEmMinutos) {
        this.duracaoEmMinutos = duracaoEmMinutos;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutoria() {
        return autoria;
    }

    public void setAutoria(String autoria) {
        this.autoria = autoria;
    }

    public int getVisualizacao() {
        return visualizacao;
    }

    public void contadorDeVisualizacao() {

        this.visualizacao = this.visualizacao + 1;
        System.out.println(visualizacao);
        //visualizacao++
    }

    public int getNumeroDeCurtidas() {
        return numeroDeCurtidas;
    }

    public void contadorDeNumeroDeCurtidas() {

        this.numeroDeCurtidas = numeroDeCurtidas + 1;

    }
}
