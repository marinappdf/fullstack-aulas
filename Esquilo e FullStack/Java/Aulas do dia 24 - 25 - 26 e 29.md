# Aula 24/01


## Recordar é viver

- ArrayList
	- como criar um vetor em java
- toString
- Entendemos melhor interface
- Construtor: é um set obrigatório
	- Recurso para poder encapsular

## Desafio: hora da prática 

Para reforçar a compreensão sobre construtores, sugerimos atividades práticas (**não obrigatórias**) que aprofundarão seus conhecimentos fundamentais.

Pronto para mergulhar nesse aprendizado prático?

1. Crie uma classe `Produto` com atributos como nome, preco, e quantidade. Em seguida, crie uma lista de objetos Produto utilizando a classe ArrayList. Adicione alguns produtos, imprima o tamanho da lista e recupere um produto pelo índice.
- Abrindo um projeto novo
- Limpar o projeto padrão
- Criar o arquivo class: botão direito + new class + nome da classe "Produto"
- Criar a class: 
```java
public class Produto { 
// isso é a nossa class
}
```
- Criar atributos
```java
public class Produto { 

// tipo de acesso + tipo da variável + nome do atributo
private String nome;
private double preco;
private int quantidade;
}
```

- Ir para o arquivo Main/Principal para criar nosso objeto e nossa lista

```java
public class Main {  
    public static void main(String[] args) {

// tipo de variável + nome da minha variável = o valor da minha variável
//int  produtoI =  9;
// o tipo de class + nome do meu objeto = criando o objeto do tipo class específica
Produto produto1 = new Produto();
Produto produto2 = new Produto();
Produto produto3 = new Produto();
Produto produto4 = new Produto();


// Agora que eu criei meus objetos, euq uero criar uma lista desses obejtos

// vou colocar o import java.util.ArrayList no topo do arquivo

// int num = 9;
// Produto produto4 = new Produto();
// estruturando a mémoria que vais usada em listaDeProdutos
ArrayList<Produto> listaDeProdutos = new ArrayList<Produto>();


listaDeProdutos.add(produto1);
listaDeProdutos.add(produto2);
listaDeProdutos.add(produto3);
listaDeProdutos.add(produto4);


//System.out.println(listaDeProdutos.get(2).getNome)
// Isso vai dar errado
// Para conseguir fazer funcionar, precisamos voltar na class Produto e criar os getter and setters
// Alt + Insert (ou botão direito + Generate) -> escolher a opção "Getter and Setters" + selecionar os atributos desejados 
// Aí sim, o sout vai funcionar

for (int i=0; i<4 ; i=i+1)  
{  
  System.out.println(i);  
  System.out.println(listaDeFilmes.get(i).getNome()); 
}



}}
 
```

# Aula 25/01 - Desafio Java

## Recordar é viver

## Apresentar o Desafio: Continuação do desafio da aula passada 


2. Implemente o método toString() na classe `Produto` para retornar uma representação em texto do objeto. Em seguida, imprima a lista de produtos utilizando o método System.out.println().

3. Modifique a classe `Produto` para incluir um construtor que aceite parâmetros para inicializar os atributos. Em seguida, crie objetos Produto utilizando esse novo construtor.

4. Crie uma classe `ProdutoPerecivel` que herde de `Produto`. Adicione um atributo dataValidade e um construtor que utilize o construtor da classe mãe (super) para inicializar os atributos herdados. Crie um objeto ProdutoPerecivel e imprima seus valores.



# Tarefa de HomeOffice 26/01 e 29/01
###  Realizar todas as Atividades do tópico 03 -  lista de objetos distintos

Link da Aula: https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126678
Também disponível no nosso plano de ensino "Aula Presencial"

No item [08 - Desafio: hora da prática](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/147673), faça pelo menos os desafios 01 e 02.

Eu estarei online e disponível para tirar dúvidas, fiquem a vontade para entrar em contato comigo.

- [01Projeto da aula anterior](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126678)
- [02Tipos diferentes de objetos na lista14min](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126228)
- [03Para saber mais: outras formas de percorrer a lista](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126679)
- [04Identificando o objeto08min](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126229)
- [05Para saber mais: variáveis e referências](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126680)
- [06Referências de objetos](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126681)
- [07Faça como eu fiz: utilizando foreach e instanceof](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126682)
- [08Desafio: hora da prática](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/147673)
- [09O que aprendemos?](https://cursos.alura.com.br/course/java-listas-colecoes-dados/task/126683)
