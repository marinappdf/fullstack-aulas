<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tabela CSS</title>
    <style>
			body {
			    margin: 0;
			    padding: 0;
			    font-family: Arial, sans-serif;
				background-color: blue;
			    color: white;
			}
			.container {
			    display: flex;
			    justify-content: center;
			    align-items: center;
			    height: 100vh;
			}
			.table {
				border-collapse: collapse;
				width: 80%;
			    text-align: center;
			    margin-top: 20px;
			}
			.table th, .table td {
			    border: 1px solid white;
			    padding: 8px;
			}
		</style>
</head>
<body>
    <div class="container">
        <table class="table">
            <tr>
                <th>Coluna 1</th>
                <th>Coluna 2</th>
                <th>Coluna 3</th>
                <th>Coluna 4</th>
            </tr>
            <tr>
                <td>Dado 1</td>
                <td>Dado 2</td>
                <td>Dado 3</td>
                <td>Dado 4</td>
            </tr>
            <!-- Adicione mais linhas conforme necessário -->
        </table>
    </div>
</body>
</html>


