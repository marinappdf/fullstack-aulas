## Recordar é viver: 

Vamos começar então revendo o que foi feito na aula anterior.

- O que vocês lembram da aula de sexta?
- Me digam 3 coisas que vocês aprenderam.

- DOM 
- document.query
### O que cada um desses comandos faz?

- ` document.querySelector('button')`
	- seleciona as tags "button"
	- seleciona a primeira aparição de "button"
- `document.querySelectorAll('button')
	- seleciona todas as aparições
- `document.getElementsByClassName('app__card-button--curto')`
	- pega o nome da classe no css
- `document.getElementsById('start-pause')`
	- pega o id
- `<script src="./script.js" defer></script>`