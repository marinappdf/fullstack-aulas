![[0. Planos de Aula/grade_curricular_polo_zaffari_v1.0.0-min.pdf]]





https://cursos.alura.com.br/formacao-certificacao-java

https://cursos.alura.com.br/formacao-java

https://cursos.alura.com.br/formacao-boas-praticas-java

https://cursos.alura.com.br/course/java-pacotes-e-java-lang

https://cursos.alura.com.br/formacao-threads-java

https://cursos.alura.com.br/course/java-consumindo-api-gravando-arquivos-lidando-erros

https://cursos.alura.com.br/formacao-certificacao-java

---


# Relatórios

Relatorios diários são registrados na pasta 0. Diários.


---

# Aprendendo Java

Estamos seguindo o curso: https://cursos.alura.com.br/formacao-java

As anotações de aula estão na pasta "Java", no documento [[Aulas de Java]].


## Aulas 

## Aulas sobre Java 16/01 a 25/01

[[Aulas de Java]]


## Aulas de JavaScript 02/01 até 15/01

[[01 - Introdução ao JavaScript]]
 [[02 - Condicional - IF]]
 [[03 - Looooops - while e for]]
[[04 - Arrayssss]]
[[05 - Objetos e Funções]]
[[JavaScript no HTML]]


## Aulas de HTML e CSS 12/12 até 02/01 

[[Currículo]]
[[Inglês instrumental]]
[[Adaptar tela em HTML]]
[[Desafio Homeoffice]]

## Aulas anteriores a 12/12

- HTML
- CSS
- DOM/JavaScript
- Introdução a programação/Lógica de programação (algoritmos, fluxogramas)
- Segurança do Trabalho
- Educação financeira
- Informático Básica
- 