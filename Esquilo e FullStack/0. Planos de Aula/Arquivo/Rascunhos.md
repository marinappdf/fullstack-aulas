## Link para o plano de aula semanal

Link para o plano de aula semanal: https://maristasorgbr.sharepoint.com/:x:/s/FullStack-social/ESL2MKtm3ylKvJSA3_BorP0B0NhH04B9-b69Z1Mq3-XOBw?email=marina.freitas%40maristas.org.br&e=4%3aYqD1SX&fromShare=true&at=9



---



Front-end


|  | Intro | https://javascript.info/ |  |
| ---- | ---- | ---- | ---- |
|  | If else |  |  |
|  | Loopings |  |  |
|  | DOM |  |  |
|  | Functions | introdução, aplicações, return console.log |  |
|  | arrays |  |  |
|  | Objetos | https://javascript.info/object |  |
|  | arrays aninhados |  |  |
|  | objetos |  |  |
|  |  |  |  |
|  | Botão |  |  |
|  | interação com o html |  |  |
|  | contas |  |  |
|  | ações |  |  |

|   |   |   |   |
|---|---|---|---|
|**41**|**React**|**18**||
|**42**|**Angular**|**18**||
|**43**|**SEO**|**8**||
|**44**|**Noções de UI/UX**|**18**||
|**45**|**Dispositivos móveis**|**18**||

Back-end

|  |  |  |  |  |
| ---- | ---- | ---- | ---- | ---- |
| **31** | **Banco de dados** | **30** |  |  |
| **32** | **API's** | **30** |  |  |
| **33** | **Containerização** | **10** |  |  |
| **34** | **Qualidade de Software** | **20** |  |  |
| **35** | **Arquitetura de Software** | **20** |  |  |
| **36** | **Linux** | **10** |  |  |
| **37** | **CI/CD** | **10** |  |  |
| **38** | **Microserviços** | **10** |  |  |
| **39** | **Cloud Computing** | **20** |  |  |
| **40** | **SRE** | **8** |  |  |


Material (Formações): 

- Formação em Front-End  - [https://www.alura.com.br/formacao-front-end](https://www.alura.com.br/formacao-front-end) 
    
- Formação SQL com MySQL Server da Oracle - [https://www.alura.com.br/formacao-oracle-mysql](https://www.alura.com.br/formacao-oracle-mysql) 
    
- Começando em DevOps - [https://www.alura.com.br/formacao-primeiros-passos-devops](https://www.alura.com.br/formacao-primeiros-passos-devops) 
    
- Formação UX Design - [https://www.alura.com.br/formacao-ux](https://www.alura.com.br/formacao-ux) 
    
- Marketing Digital - [https://www.alura.com.br/formacao-marketing-digital](https://www.alura.com.br/formacao-marketing-digital)

---


Material do jean:
- https://maristasorgbr-my.sharepoint.com/personal/carolina_faustino_maristas_org_br/_layouts/15/onedrive.aspx?fromShare=true&ga=1&id=%2Fpersonal%2Fcarolina%5Ffaustino%5Fmaristas%5Forg%5Fbr%2FDocuments%2F2022%2F01%20%2D%20REGISTRO%20DI%C3%81RIO%20DE%20APRENDIZAGEM%20%28RDA%29%2FFULL%20STACK%20SOCIAL%20JUL%2E22%20TARDE%2FJean%20Silveira%20da%20Costa&sortField=Modified&isAscending=true

Em setembro começa o javascript


em outubro ux usability
