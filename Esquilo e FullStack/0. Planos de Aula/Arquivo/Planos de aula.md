
terça - finalizar o currículo: tem que ter um footer, tem que estar bonito
quarta - aula sobre atalhos de computador + outras coisas, colocar botões no currículo
quinta - fazer uma grid e manipular ela 
sexta - [[Gestão de produtos]]

[[Gestão de produtos - Estrutura de times de produtos]]

### Semana 1

| Dia semana| Data| Conteúdo|
|-|-|-|
|Ter|12 Dez | [[Consciência Política]], [[ECA]], [[Políticas de segurança pública voltadas para adolescentes e jovens]]||
|Qua|13 Dez|[[Mapeando o conhecimetno]]|
|Qui|14 Dez |[[CSS externo]], [[Front-end/Html e CSS/Currículo com Html/Currículo 1]]|
|Sex|15 Dez|[[Inglês instrumental]] |

### Semana 2


| Dia semana| Data| Conteúdo|
|-|-|-|
|Seg | 18 Dez| |
|Ter|19 Dez ||
|Qua|20 Dez| |
|Qui|21 Dez ||
|Sex|22 Dez| Home-office - Trabalho 1|

### Aulas prontas ainda não aplicadas

[[Saúde e Segurança do Trabalho]]

[[Gestão de produtos]]

[[Gestão de produtos - Estrutura de times de produtos]]