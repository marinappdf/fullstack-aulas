
![[grade curricular full stack.pdf]]
# Relatórios

Relatórios estão nas notas diários na pasta 0. Diários

---

# Planos de Aula

Conteúdos de Front-end

| DOM  |
|---|
|**React**|
|**Angular**|
|**SEO**|
|**Noções de UI/UX**|
|**Dispositivos móveis**|

Conteúdo Básico

- Comunicação
- Leitura e interpretação
- Educação sexual
- Direitos humanos
- Projeto de vida
- Diversidade cultural
- 


---

# Aula  30/01 - Apresentação do Seminário


---

# Aula 26/01 e 29/01 - Homeoffice

- Criando um Seminário
- Em duplas, cada dupla com um tema.
- No mínimo 10 Slides, sem contar capa e agradecimentos
- Apresentar um exemplo de código, explicar ele, se possível, fazer algo rodar

Temas: Bibliotecas e frameworks do java script

- Bibliotecas
	- jQuery
	- D3.JS
	- Glimmer.JS
	- Babel
- Frameworks JavaScript 
	- Bootstrap
	- AngularJS
	- Angular (2-9)
	- Ember.js
	- Vue.js
 


Responder as seguintes perguntas:
1. O que são Bibliotecas e Frameworks JavaScript?
2. Qual biblioteca/framework será apresentado?
3. Porque e quando ele foi criado?
4. Quais problemas ele resolve? Qual o seu objetivo? Quais são suas vantagens?
5. O que ele traz de novo? É preciso instalar algo para o seu uso? Podemos usar o Visualcode? Existe agum diferencial na sua linguagem?
6. Exemplo de código: qual a cara dele?
7. Mini-projeto usando essa biblioteca/framework (opcional).






---

# Aula 25/01 - Orientação, acompanhamento

Parte 1: Finalizando  o REACT

Parte 2: Orientação Individual

---

# Aulas 19/01 a  24/01 - React

[[REACT]]

---

# Aula 18/01 - Apresentação do seminário + Aula de REACT


- [[REACT#Aula 1 - Introduzindo tema]]



---
## AULA 17/01 - PESQUISAS SOBRE [[DOM]] E [[REACT]]

Nessa aula vamos fazer um seminários sobre os temas que trabalhamos e vamos trabalhar.
Teremos dois grupos pesquisando sobre assuntos relacionados com DOM e árvores no HTML, e outros grupos pesquisando sobre REACT.

A primeira metade da aula deve ser a pesquisa, e a segunda metade deve ser de apresentação.
Pensei apresentações de 10 minutos. Assim, 9 grupos significa 90min de apresentação, ou seja, 1h30.

Vou enviar os links que são os temas de cada grupos, você devem se manter no escopo do link, mas podem pesquisar em amterial externo.

Grupo 1
- https://javascript.info/dom-nodes
- https://javascript.info/searching-elements-dom
- José e Rafael
- 
Grupo 2
	- https://javascript.info/basic-dom-node-properties
	- https://javascript.info/dom-attributes-and-properties
	- Gabriel e Gabriel

Grupo 3
	- https://javascript.info/styles-and-classes
	- https://javascript.info/size-and-scroll
		- Alessandra e Michel

Grupo 4
	- https://javascript.info/size-and-scroll-window
	- https://javascript.info/coordinates
	- Alexsander e Everton

Grupo 5
 - O que é REACT? Introdução e usos
 - https://react.dev/learn
 - Pedro, Elysa e Alex
Grupo 6
	-O que é REACT? Introdução e usos
	 - https://react.dev/learn
	 - Gustavo e João
Grupo 7
- Começando com REACT
	- https://react.dev/learn/your-first-component
	- Yuri e Werick
Grupo 8 
- Escrevendo markup com jsx
- https://react.dev/learn/writing-markup-with-jsx
- Wesley e Micael











----
## Aula 16/01: [[DOM]]

Plano de aulas em [[DOM#Aula 3 - 16/01]]







