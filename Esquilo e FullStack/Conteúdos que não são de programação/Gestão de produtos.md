
----

## Gestão de Produtos

#### Marina de Freitas

##### @marifreitas___

---

## Gestão de Produtos

###
###
###

##### (Mapa Mental)


<!--
Qual o produto que estamos aprendendo a criar?
* Sofwares
* Páginas web
* Banco de dados
* Códigos de computador

Quais as pessoas estão relacionadas nessa gestão?
* Coordenadores
* Cliente
* Consumidor
* Colegas de desenvolvimento
* Designers
* Marketing
* Advogados
* Artistas
* Publicitários
-->


---


### GP = **Ciclo de vida do produto**

#### Nasce, vive, se reproduz, morre

### =

#### Ideia, Pesquisa de Mercado, Desenvolvimento, Testes, Design, Publicidade, Atualização


---

![[gestao-de-produtos-3-4.png]]

---

### Atividade de hoje:

### Simulação de gestão de projetos
##### Em grupos, vamos simular um processo de gestão de produtos por metodologias ágeis

##### Qual o produto?

####

##### (e o que é isso?)


---

### O Processo de Gestão de Produtos

1. Identificação de oportunidades de mercado
1. Desenvolvimento de estratégias de produto
1. Definição de requisitos e especificações
1. Desenvolvimento e lançamento do produto
1. Monitoramento e melhoria contínua

- Exemplo: uma tesoura
<!--
Discussão similar a do slide anterior
-->

---

### Etapa 1: Identificação de Oportunidades de Mercado

1. Pesquisa de mercado
1. Análise da concorrência
1. Entrevistas com clientes
1. Identificação de tendências

#### 

 - Exemplo: Tesouro escolar? Tesoura para costura? Tesoura para plástico?
#####
###### (melhor copiar para usar depois)

---

### Etapa 2: Desenvolvimento de Estratégias de Produto

1. Definição do posicionamento do produto
1. Definição do público-alvo
1. Definição dos objetivos de marketing
1. Definição do mix de marketing

#####

- Ex: como vai ser a apresentação desse produto?

<!--
-->

---

### Etapa 3: Definição de Requisitos e Especificações

1. Levantamento de requisitos dos clientes
1. Definição das especificações técnicas
1. Estabelecimento de metas de qualidade
1. Documentação dos requisitos e especificações

Ex.: tesoura escolar -> sem ponta, adequada para o tamanho dos dedos das crianças, segurança, plástico não tóxico
<!--
-->

---

### Etapa 4: Desenvolvimento e Lançamento do Produto

1. Projeto e desenvolvimento do produto
1. Testes e validação
1. Produção em massa
1. Lançamento no mercado

######

- Ex.: como testar esse produto? testar direto com o cliente? grupo focal?
<!--

-->

---

#### Etapa 5: Monitoramento e Melhoria Contínua

- Acompanhamento das vendas e desempenho do produto
- Coleta de feedback dos clientes
- Análise de dados e métricas
- Implementação de melhorias e atualizações

####

- Ex.: entrevista com prof. para retorno sobre o desempenho do produto.
<!--

-->


----

# Parte 2 da aula

---

# Metodologias Ágeis
##### (para a gestão de produtos)


---
### Metodologias ágeis são formas de melhorar a gestão de produtos

- Entrega contínua de valor: estar pronto para uso antes de estar totalmente finalizado, atualizações contínuas
- Adaptabilidade: reuniões curtas e constantes permitem a a rápida correção e mudança de rumos do projeto
- Maior colaboração e transparência 

---
## Ferramentas de metodologias ágeis
---
#### Ferramentas de metodologias ágeis

- Scrum
- Kanban
- Lean
- Extreme Programming
- Agile Manifesto

---
#### Cada grupo pode escolher uma metodologia para preparar uma mini apresentação sobre ela

##### No buscador, pesquise por "metodologia scrum".

##### Busque entender mais ou menos como funciona a metodologia.

##### Em seguida, tente resumir o que a metodologia faz e mostre para os colegas.

##### Você pode fazer 5 slides, resumir tudo em um arquivo de texto, ou usar o quadro branco.



---
# Conclusão

- Recapitulação do processo de gestão de produtos
- Importância da gestão de produtos para o sucesso das empresas
- O que vocês acharam?

<!--
-->

---

# Gracias!




---

#### Referência

https://rockcontent.com/br/blog/gestao-de-produtos/