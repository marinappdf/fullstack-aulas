

# Saúde e Segurança do Trabalho

---

# Para que serve?


---

### Top 10 acidentes: https://www.youtube.com/watch?v=r68ku5RhKAA

### Campanha de prevenção: https://www.youtube.com/watch?v=3K1tZoB58Dg

### 25 Motivos: https://www.youtube.com/watch?v=ov07Z2sHYJE



---


### Acidentes de trabalho são comuns por que as pessoas acreditam
### que nunca vai acontecer com elas.
#### Na rotina, vamos escolher o caminho mais fácil e prático, não necessariamente o mais seguro.

---

## Estatísticas de Acidentes

- Números de acidentes de trabalho
- Causas mais comuns de acidentes
- Impacto dos acidentes na produtividade


<!--
    Em 2021, o Brasil registrou 536.174 acidentes de trabalho, um aumento de 15,11% em relação ao ano anterior, quando foram registradas 465.772 ocorrências1.
    O número de acidentes de trabalho que resultaram em tratamento por período maior de 15 dias foi de 133.757 e foram contabilizados 1.694 óbitos2.
    Em 2022, o Brasil registrou 612.900 acidentes de trabalho e 2.538 mortes, um aumento de 22% em relação ao ano anterior3.
    Apesar desses números, houve uma queda de 25,6% nos acidentes de trabalho no Brasil nos últimos 10 anos4.
    Em 2021, os homens representaram 65,8% das vítimas de acidentes de trabalho, enquanto as mulheres representaram 34,2%4.
    Contrariando a tendência de alta observada para a maioria dos dados de acidentes, houve uma queda na incidência de doenças ocupacionais em 42,37%, passando de 33.575 declarações em 2020 para 19.348 em 20211.
--> 

---

## Equipamentos de Proteção Individual (EPIs)

- **E P Is**
	- são essenciais para prevenir **acidentes** e **doenças ocupacionais** 
- Tipos de EPIs: depende da área do trabalho
- Quais vocês imaginam que são os EPIs de um trabalhador 
	- da Enfermagem?
	- da construção civil?
	- da programação de software?

<!--
- Lesão por Esforço Repetitivo (LER)
- Distúrbios Osteomusculares Relacionados ao Trabalho (DORT)
- Transtornos mentais relacionados ao trabalho (TMRT)
- Asma ocupacional
- Dermatite de contato ocupacional
- Surdez induzida por ruído
- [Câncer ocupacional](https://www.conexasaude.com.br/blog/doenca-ocupacional/)[3](https://supersipat.com.br/doenca-ocupacional/)-->

---

## EPI não é a única proteção
#
#
## A ambiente de trabalho deve também ser
## projetado para a segurança do trabalhador
#
#
## O que isso significa?

##### (O ambiente de trabalha não deve ser perigosa como a selva)

---

# Normas Regulamentadoras 
## CLT: Consolidação das Leis do Trabalho

- Determinam as especificações de segurança dos produtos:
	- O que a máquina ou  EPI deve e não deve ter para ser seguro
- Determina as condições de trabalho
	- O que deve e não deve ter no ambiente para ele ser saudável e seguro
- Determinam as responsabilidades de cada um para garantir a segurança e saúde do trabalhador:
	- Do empregador: fornecer EPIs e condições de trabalho adequadas, treinamentos e capacitações, manutenção e higienização periódicas, análise de riscos e prevenção de acidentes etc.
	- Do trabalhador: usar os EPIs e máquinas conforme solicitado, manutenção e cuidado, informar quando danificado, etc.



---

## Qualidade de vida no trabalho
## Saúde laboral

- A Organização Mundial de Saúde (OMS)
	- **"um estado de completo bem-estar físico, mental e social, e não meramente a ausência de doença"**
	- local de trabalho saudável: livre de acidentes e doenças profissionais, e que promove o bem-estar geral dos trabalhadores


---

## O que pode gerar um ambiente de trabalho não saudável?
#
#
## Vocês já trabalharam ou estudaram 
## em ambientes não saudáveis?

#
#
#### (Teatro para não atores)