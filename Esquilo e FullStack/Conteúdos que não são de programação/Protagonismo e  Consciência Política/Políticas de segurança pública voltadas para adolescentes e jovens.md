
---
# Políticas de segurança pública 
# voltadas para adolescentes e
# jovens

---

### Políticas de segurança pública voltadas para adolescentes e jovens
#
#
## A quais riscos vocês acham que vocês estão expostos?
#
#
#
## Vocês acham que existem leis que protegem vocês?


---

## Desafios enfrentados pelos adolescentes

- Vulnerabilidade a influências negativas
- Risco de envolvimento em atividades criminosas
- Exposição a violência e abuso
- O que mais?


---

## Objetivos das políticas de segurança
## pública para adolescentes

- Prevenção do envolvimento em atividades criminosas
- Proteção contra violência e abuso
- Promoção de oportunidades de desenvolvimento saudável


---

## Estratégias de implementação

- Educação e conscientização
- Acesso a programas de apoio e orientação
- Parcerias com instituições e comunidades locais
- Vocês acham que já aplicaram alguma dessas estratégias com vocês?

---
## Resultados esperados

- Redução da criminalidade juvenil
- Melhoria da qualidade de vida dos adolescentes
- Fortalecimento da comunidade

---

## Exemplo de programa de segurança 
## pública para adolescentes
####
####

####
### Estratégias para a prevenção de crimes
- Atividades esportivas e culturais
- Programas de mentoria
- Acesso a serviços de saúde mental

#####
### Como essas ações atuam na prevenção de crimes?

---

# Atividade: 
**Em pequenos grupos, identifiquem programas, políticas ou atividades que vocês viveram/fizeram/viram/conhecem que podem ser consideradas políticas de segurança pública voltadas para adolescentes e jovens
**
