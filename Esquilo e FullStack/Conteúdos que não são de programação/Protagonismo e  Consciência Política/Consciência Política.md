---
aliases:
---

# Consciência Política


---

## O que vocês entendem por Consciência Política?


---

## O que é ter consciência sobre algo?

---

## Consciência política: 

- Conhecimento sobre o **sistema** *político*
- Compreensão dos direitos e deveres cívicos
- Capacidade de análise crítica das políticas públicas
- Participação ativa na vida política
- Tomada de decisões informadas
- Influência na formulação de políticas públicas

---

## Vocês acham que tem consciência política?
#
#

### Numa escala de 1 à 5?

---

## Vamos assistir esse vídeo:
#
### https://www.youtube.com/watch?v=EuUN_t32YCw
#
### Anotem o que mais chamou atenção de vocês

---

## Dito isso,
#
## Qual é a sua leitura de mundo?
#
## Qual o meu lugar no mundo?
#
## Quais são os meus marcadores sociais?

---

# Querem ver mais?

## https://www.youtube.com/watch?v=OCPzKtsbHtA

---
