
# Estatuto da Criança 
# e da Adolescência

---
## Introdução
#
#

- O que é o Estatuto da Criança e da Adolescência (ECA)?
- Autoria: Congresso Nacional
- Propósito: "manual" jurídico para a proteção da criança e do adolescente

---

## Objetivos do ECA
#
#
- Garantir os direitos fundamentais das crianças e adolescentes.
- Proteger contra qualquer forma de violência, abuso ou exploração.
- Promover o desenvolvimento integral e saudável.

---

## Princípios do ECA

- Prioridade absoluta: todos devem priorizar o cuidado das crianças e dos adolescentes
- Proteção integral: têm todos os direitos de uma pessoa humana garantidos, não são considerados "inferiores", nem "secundários"
- Participação: direito a se expressarem e serem ouvidas
- Respeito à dignidade: merecem ter direitos garantidos


---

## Direitos das Crianças e Adolescentes

- Direito à vida, saúde e alimentação -> Saúde pública
- Direito à educação e cultura -> Escolas e lazer gratuitos
- Direito à convivência familiar e comunitária -> segurança pública, justiça social
- Direito à proteção contra qualquer forma de violência -> incluído dos próprios tutores

---

## Medidas de Proteção

- Acolhimento institucional.
- Apoio psicossocial.
- Medidas socioeducativas.


---

## Responsabilidades dos Pais e Responsáveis

- Garantir os direitos e o desenvolvimento dos filhos.
- Prover condições de vida adequadas.
- Zelar pela saúde e educação.

---

## Responsabilidades do Estado

- Garantir políticas públicas voltadas para a infância e adolescência.
- Fiscalizar e punir violações dos direitos das crianças e adolescentes.
- Promover a participação e o protagonismo dos jovens.

---

# Quando, na sua vida,
# você fez uso do ECA?

---

## Conclusão

- O ECA é uma legislação fundamental para a proteção dos direitos das crianças e adolescentes.
- É responsabilidade de todos garantir o cumprimento do ECA e promover o bem-estar das crianças e adolescentes.

---